//
//  ModalViewController.h
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIImageView *adlDesc,*behDesc,*chcDesc;

- (IBAction)dismissAction:(UIButton*)sender;

@end
