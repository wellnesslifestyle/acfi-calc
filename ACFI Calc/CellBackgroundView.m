//
//  CellBackgroundView.m
//  fgawesgews
//
//  Created by V.NET on 10/4/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "CellBackgroundView.h"

static const CGFloat kCornerRadius = 10;

@implementation CellBackgroundView

@synthesize position, fillColor, borderColor;


//overriding the drawRec function in UIView Class to suit my customize table cells in CalcView Page
- (void)drawRect:(CGRect)rect
{
    
    CGRect bounds = CGRectInset(self.bounds,
                                0.5 / [UIScreen mainScreen].scale,
                                0.5 / [UIScreen mainScreen].scale);
    UIBezierPath *path;
    if (position == CellPositionSingle) {
        path = [UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:kCornerRadius];
    } else if (position == CellPositionTop) {
        bounds.size.height += 1;
        path = [UIBezierPath bezierPathWithRoundedRect:bounds
                                     byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                           cornerRadii:CGSizeMake(kCornerRadius, kCornerRadius)];
    } else if (position == CellPositionBottom) {
        path = [UIBezierPath bezierPathWithRoundedRect:bounds
                                     byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                           cornerRadii:CGSizeMake(kCornerRadius, kCornerRadius)];
    } else {
        bounds.size.height += 1;
        path = [UIBezierPath bezierPathWithRect:bounds];
    }
    
    [self.fillColor setFill];
    [self.borderColor setStroke];
    [path fill];
    [path stroke];
}

@end

 /* my sample usage
 
CellBackgroundView *backgroundView = [CellBackgroundView new];
cell.backgroundView = backgroundView;
backgroundView.backgroundColor = [UIColor clearColor];
backgroundView.borderColor = self.tableView.separatorColor;
backgroundView.fillColor = [UIColor colorWithWhite:137/255. alpha:1.0];

int rowsInSection = [self tableView:tableView numberOfRowsInSection:indexPath.section];
if (rowsInSection == 1) {
    backgroundView.position = CellPositingSingle;
} else {
    if (indexPath.row == 0) {
        backgroundView.position = CellPositionTop;
    } else if (indexPath.row == rowsInSection - 1) {
        backgroundView.position = CellPositionBottom;
    } else {
        backgroundView.position = CellPositionMiddle;
    }
} */
