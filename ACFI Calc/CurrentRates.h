//
//  ADL_BEH_Rates.h
//  fgawesgews
//
//  Created by V.NET on 9/23/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentRates : NSObject

@property (strong,nonatomic) NSString *myRate;
@property (assign,nonatomic) float amount;

-(id)initWithRate:(NSString*)myRate andAmount:(float)myValue;

@end
