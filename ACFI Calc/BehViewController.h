//
//  BehViewController.h
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataStorageClass.h"


@interface BehViewController : UIViewController

@property (strong,nonatomic) DataStorageClass *myDataStorage;

-(IBAction) tapButtons:(UIButton*)sender;
-(void) setQuestionPoints:(int)qIndex andChoices:(int)cIndex;
-(void)determineCategoryAndAmount;
//-(IBAction)tapBackButton:(UIButton*)sender;
//-(IBAction)goToChcVC:(UIButton*)sender;
@end

