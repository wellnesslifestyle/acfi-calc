//
//  UIPopoverController+IPhonePopover.m
//  fgawesgews
//
//  Created by V.NET on 9/24/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "UIPopoverController+IPhonePopover.h"

@implementation UIPopoverController (IPhonePopover)

//this function will force the popover to run on iPhone
+(BOOL)_popoversDisabled{
    return NO;
}

@end
