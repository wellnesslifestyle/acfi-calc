//
//  ADL_BEH_Rates.m
//  fgawesgews
//
//  Created by V.NET on 9/23/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "CurrentRates.h"

@implementation CurrentRates

-(id)initWithRate:(NSString *)rate andAmount:(float)myValue
{
    if(self=[super init]){
        _myRate = rate;
        _amount = myValue;
    }
    
    return self;
}


@end
