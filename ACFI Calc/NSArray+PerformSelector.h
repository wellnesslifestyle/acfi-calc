//
//  NSArray+PerformSelector.h
//  ACFI Calc
//
//  Created by V.NET on 12/1/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (PerformSelector)

- (NSArray *)arrayByPerformingSelector:(SEL)selector;
@end
