//
//  ModalViewController.m
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "ModalViewController.h"


@interface ModalViewController ()
{
    IBOutlet UIButton *doneButton;
}
@end

@implementation ModalViewController


#pragma mark - view controller life cycle
- (void)viewDidLoad
{
        
	
	[self.adlDesc setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"infoHolder"]]];
    [self.behDesc setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"infoHolder"]]];
    [self.chcDesc setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"infoHolder"]]];
    
    //set done button
    UIImage *selState = [UIImage imageNamed:@"button_done_selected"];
    UIImage *unselState = [UIImage imageNamed:@"button_done_unselected"];
    [doneButton setBackgroundImage:unselState forState:UIControlStateNormal];
    [doneButton setBackgroundImage:selState forState:UIControlStateHighlighted];
    [doneButton addTarget:self action:@selector(dismissAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPHONE5)
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor-568h"]];
    else
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor"]];

}


- (id)init
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        //If it is at least 6.0, it loads the nib with Auto Layout enabled.
        self = [super initWithNibName:@"ModalViewController" bundle:nil];
    }
    else
    {
        NSLog(@"version 5 to 5.9");
        self = [super initWithNibName:@"ModalViewController-legacy" bundle:nil];
    }
    
    return self;
}

- (IBAction)dismissAction:(UIButton*)sender{
    
	[self dismissModalViewControllerAnimated:YES];
}

@end
