//
//  AdlViewController.m
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "AdlViewController.h"
#import "DataStorageClass.h"
#import "DomainQuestionClass.h"
#import "FundingClass.h"
#import "CalcViewController.h"
#import "DataAPI.h"
#import "MyButton.h"

@interface AdlViewController ()
{
    IBOutlet UIImageView *buttonHolder;
    IBOutlet UIImageView *displayHolder;
    IBOutlet UILabel *rateLabel;
    IBOutlet UILabel *categoryLabel;
    
    //nutrition
    IBOutlet UIButton *button1;
    IBOutlet UIButton *button2;
    IBOutlet UIButton *button3;
    IBOutlet MyButton *button4;
    
    //mobility
    IBOutlet UIButton *button5;
    IBOutlet UIButton *button6;
    IBOutlet UIButton *button7;
    IBOutlet UIButton *button8;
    
    //personal hygiene
    IBOutlet UIButton *button9;
    IBOutlet UIButton *button10;
    IBOutlet UIButton *button11;
    IBOutlet UIButton *button12;
    
    //toileting
    IBOutlet UIButton *button13;
    IBOutlet UIButton *button14;
    IBOutlet UIButton *button15;
    IBOutlet UIButton *button16;
    
    //continence
    IBOutlet UIButton *button17;
    IBOutlet UIButton *button18;
    IBOutlet UIButton *button19;
    IBOutlet UIButton *button20;
//    UIImageView *selected,*unSelected;
    
    
}
@property (nonatomic,assign) float Q1,Q2,Q3,Q4,Q5;
@property (nonatomic,assign) float totalResult;

@end



@implementation AdlViewController

#pragma mark - initialization
- (id)init
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        //If it is at least 6.0, it loads the nib with Auto Layout enabled.
        self = [super initWithNibName:@"AdlViewController" bundle:nil];
    }
    else
    {
        NSLog(@"version 5 to 5.9");
        self = [super initWithNibName:@"AdlViewController-legacy" bundle:nil];
    }
    
    if(self){
        self.title = @"ADL";
        _totalResult = _Q1 = _Q2 = _Q3 = _Q4 = _Q4 = 0.0;
        _myDataStorage = [[DataStorageClass alloc] init];
    }
    
    return self;
    
}


#pragma mark - view controller life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self customizedMyInterface];
    
}

#pragma mark - memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - private function for ui customization
-(void)customizedMyInterface{
     //set the page background color
    
    if (IS_IPHONE5)
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor-568h"]];
    else
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor.png"]];
    
    UIImage *selA,*selB,*selC,*selD,*unselA,*unselB,*unselC,*unselD;
    selA = [UIImage imageNamed:@"selectA"];
    selB = [UIImage imageNamed:@"selectB"];
    selC = [UIImage imageNamed:@"selectC"];
    selD = [UIImage imageNamed:@"selectD"];
    unselA = [UIImage imageNamed:@"unselectA"];
    unselB = [UIImage imageNamed:@"unselectB"];
    unselC = [UIImage imageNamed:@"unselectC"];
    unselD = [UIImage imageNamed:@"unselectD"];
    
    //set button initial state color
    button1.selected = button5.selected = button9.selected = button13.selected = button17.selected = YES;
    if([button1 isSelected] && [button5 isSelected] && [button9 isSelected] && [button13 isSelected] && [button17 isSelected])
    {
        [button1 setBackgroundImage:selA forState:UIControlStateNormal];
        [button5 setBackgroundImage:selA forState:UIControlStateNormal];
        [button9 setBackgroundImage:selA forState:UIControlStateNormal];
        [button13 setBackgroundImage:selA forState:UIControlStateNormal];
        [button17 setBackgroundImage:selA forState:UIControlStateNormal];
        _Q1 = _Q2 = _Q3 = _Q4 = _Q5 = 0.0f;
    }
    [button2 setBackgroundImage:unselB forState:UIControlStateNormal];
    [button3 setBackgroundImage:unselC forState:UIControlStateNormal];
    [button4 setBackgroundImage:unselD forState:UIControlStateNormal];

    [button6 setBackgroundImage:unselB forState:UIControlStateNormal];
    [button7 setBackgroundImage:unselC forState:UIControlStateNormal];
    [button8 setBackgroundImage:unselD forState:UIControlStateNormal];

    [button10 setBackgroundImage:unselB forState:UIControlStateNormal];
    [button11 setBackgroundImage:unselC forState:UIControlStateNormal];
    [button12 setBackgroundImage:unselD forState:UIControlStateNormal];

    [button14 setBackgroundImage:unselB forState:UIControlStateNormal];
    [button15 setBackgroundImage:unselC forState:UIControlStateNormal];
    [button16 setBackgroundImage:unselD forState:UIControlStateNormal];

    [button18 setBackgroundImage:unselB forState:UIControlStateNormal];
    [button19 setBackgroundImage:unselC forState:UIControlStateNormal];
    [button20 setBackgroundImage:unselD forState:UIControlStateNormal];
    
    //set screen display and button holder
    [displayHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"holder"]]];
    [buttonHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"buttonHolder"]]];
    

    rateLabel.text = [NSString stringWithFormat:@"$0.00"];
//    self.navigationItem.title = @"New ACFI Rate";
    
    //customized my left bar button
//    UIImage *buttonSelected = [UIImage imageNamed:@"bar_left_buttonSelected"];
//    UIImage *buttonUnselected = [UIImage imageNamed:@"bar_left_buttonUnselected"];
//    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [myCustomButton setBackgroundImage:buttonSelected forState:UIControlStateHighlighted];
//    [myCustomButton setBackgroundImage:buttonUnselected forState:UIControlStateNormal];
//    myCustomButton.frame = CGRectMake(0.0f, 0.0f,buttonUnselected.size.width,buttonUnselected.size.height);
//    [myCustomButton addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *myDoneButton = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
//    self.navigationItem.leftBarButtonItem = myDoneButton;
    
//    customized my right bar button
//    UIImage *rightButtonSelected = [UIImage imageNamed:@"bar_right_buttonSelected"];
//    UIImage *rightButtonUnselected = [UIImage imageNamed:@"bar_right_buttonUnselected"];
//    UIButton *modalViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [modalViewButton setBackgroundImage:rightButtonUnselected forState:UIControlStateNormal];
//    [modalViewButton setBackgroundImage:rightButtonSelected forState:UIControlStateHighlighted];
//    [modalViewButton addTarget:self
//                        action:@selector(goToBehVC:)
//              forControlEvents:UIControlEventTouchUpInside];
//    modalViewButton.frame = CGRectMake(0.0f, 0.0f,rightButtonUnselected.size.width,rightButtonUnselected.size.height);
//	UIBarButtonItem *modalBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
//	self.navigationItem.rightBarButtonItem = modalBarButtonItem;

}

-(IBAction)goToBehVC:(UIButton*)sender{
    
    BehViewController *behVC = [[BehViewController alloc] init];
    
    //checks if the navigation controller exists in view controller
    if (self.navigationController != nil){
        [self.navigationController pushViewController:behVC animated:YES];
    }
}

-(IBAction)done:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    ////this checking will notify the delegate if it exists.
//    if (self.delegate != nil) {
//        [_delegate adlViewController:self didFinishSelectingRate:_amountFunding];
//    }
}

//this function is the event handler of the button that will change the color when it is selected
-(IBAction)tapButtons:(UIButton *)sender
{
    UIImage *selA,*selB,*selC,*selD,*unselA,*unselB,*unselC,*unselD;
    selA = [UIImage imageNamed:@"selectA"];
    selB = [UIImage imageNamed:@"selectB"];
    selC = [UIImage imageNamed:@"selectC"];
    selD = [UIImage imageNamed:@"selectD"];
    unselA = [UIImage imageNamed:@"unselectA"];
    unselB = [UIImage imageNamed:@"unselectB"];
    unselC = [UIImage imageNamed:@"unselectC"];
    unselD = [UIImage imageNamed:@"unselectD"];
    
    if(sender.tag >= 1 && sender.tag <= 4){
        switch (sender.tag) {
            case 1:
                button1.selected = YES;
                button2.selected = button3.selected = button4.selected = NO;
                [button2 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button3 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button4 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button1 isSelected])
                    [button1 setBackgroundImage:selA forState:UIControlStateNormal];
                break;
            case 2:
                button2.selected = YES;
                button1.selected = button3.selected = button4.selected = NO;
                [button1 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button3 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button4 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button2 isSelected])
                    [button2 setBackgroundImage:selB forState:UIControlStateNormal];
                break;
            case 3:
                button3.selected = YES;
                button1.selected = button2.selected = button4.selected = NO;
                [button1 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button2 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button4 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button3 isSelected])
                    [button3 setBackgroundImage:selC forState:UIControlStateNormal];
                break;
            case 4:
                button4.selected = YES;
                button1.selected = button2.selected = button3.selected = NO;
                [button1 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button2 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button3 setBackgroundImage:unselC forState:UIControlStateNormal];
                if ([button4 isSelected])
                    [button4 setBackgroundImage:selD forState:UIControlStateNormal];
                break;
        }
        
        [self setQuestionPoints:0 andChoices:sender.tag];
        
    }else if(sender.tag >= 5 && sender.tag <= 8){
        switch (sender.tag) {
            case 5:
                button5.selected = YES;
                button6.selected = button7.selected = button8.selected = NO;
                [button6 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button7 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button8 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button5 isSelected])
                    [button5 setBackgroundImage:selA forState:UIControlStateNormal];
                break;
            case 6:
                button6.selected = YES;
                button5.selected = button7.selected = button8.selected = NO;
                [button5 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button7 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button8 setBackgroundImage:unselD forState:UIControlStateNormal];
                if([button6 isSelected])
                    [button6 setBackgroundImage:selB forState:UIControlStateNormal];
                break;
            case 7:
                button7.selected = YES;
                button5.selected = button6.selected = button8.selected = NO;
                [button5 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button6 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button8 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button7 isSelected])
                    [button7 setBackgroundImage:selC forState:UIControlStateNormal];
                break;
            case 8:
                button8.selected = YES;
                button5.selected = button6.selected = button7.selected = NO;
                [button5 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button6 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button7 setBackgroundImage:unselC forState:UIControlStateNormal];
                if ([button8 isSelected])
                    [button8 setBackgroundImage:selD forState:UIControlStateNormal];
                break;
        }
        
        [self setQuestionPoints:1 andChoices:sender.tag-4];
        
    }else if(sender.tag >= 9 && sender.tag <= 12){
        switch (sender.tag) {
            case 9:
                button9.selected = YES;
                button10.selected = button11.selected = button12.selected = NO;
                [button10 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button11 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button12 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button9 isSelected])
                    [button9 setBackgroundImage:selA forState:UIControlStateNormal];
                break;
            case 10:
                button10.selected = YES;
                button9.selected = button11.selected = button12.selected = NO;
                [button9 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button11 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button12 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button10 isSelected])
                    [button10 setBackgroundImage:selB forState:UIControlStateNormal];
                break;
            case 11:
                button11.selected = YES;
                button9.selected = button10.selected = button12.selected = NO;
                [button9 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button10 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button12 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button11 isSelected])
                    [button11 setBackgroundImage:selC forState:UIControlStateNormal];
                break;
            case 12:
                button12.selected = YES;
                button9.selected = button10.selected = button11.selected = NO;
                [button9 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button10 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button11 setBackgroundImage:unselC forState:UIControlStateNormal];
                if ([button12 isSelected])
                    [button12 setBackgroundImage:selD forState:UIControlStateNormal];
                break;
        }
        
        [self setQuestionPoints:2 andChoices:sender.tag-8];
        
    }else if(sender.tag >= 13 && sender.tag <= 16){
        switch (sender.tag) {
            case 13:
                button13.selected = YES;
                button14.selected = button15.selected = button16.selected = NO;
                [button14 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button15 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button16 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button13 isSelected])
                    [button13 setBackgroundImage:selA forState:UIControlStateNormal];
                break;
            case 14:
                button14.selected = YES;
                button13.selected = button15.selected = button16.selected = NO;
                [button13 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button15 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button16 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button14 isSelected])
                    [button14 setBackgroundImage:selB forState:UIControlStateNormal];
                break;
            case 15:
                button15.selected = YES;
                button13.selected = button14.selected = button16.selected = NO;
                [button13 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button14 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button16 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button15 isSelected])
                    [button15 setBackgroundImage:selC forState:UIControlStateNormal];
                break;
            case 16:
                button16.selected = YES;
                button13.selected = button14.selected = button15.selected = NO;
                [button13 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button14 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button15 setBackgroundImage:unselC forState:UIControlStateNormal];
                if ([button16 isSelected])
                    [button16 setBackgroundImage:selD forState:UIControlStateNormal];
                break;
        }
        [self setQuestionPoints:3 andChoices:sender.tag-12];
    
    }else{
        switch (sender.tag) {
            case 17:
                button17.selected = YES;
                button18.selected = button19.selected = button20.selected = NO;
                [button18 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button19 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button20 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button17 isSelected])
                    [button17 setBackgroundImage:selA forState:UIControlStateNormal];
                break;
            case 18:
                button18.selected = YES;
                button17.selected = button19.selected = button20.selected = NO;
                [button17 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button19 setBackgroundImage:unselC forState:UIControlStateNormal];
                [button20 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button18 isSelected])
                    [button18 setBackgroundImage:selB forState:UIControlStateNormal];
                break;
            case 19:
                button19.selected = YES;
                button17.selected = button18.selected = button20.selected = NO;
                [button17 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button18 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button20 setBackgroundImage:unselD forState:UIControlStateNormal];
                if ([button19 isSelected])
                    [button19 setBackgroundImage:selC forState:UIControlStateNormal];
                break;
            case 20:
                button20.selected = YES;
                button17.selected = button18.selected = button19.selected = NO;
                [button17 setBackgroundImage:unselA forState:UIControlStateNormal];
                [button18 setBackgroundImage:unselB forState:UIControlStateNormal];
                [button19 setBackgroundImage:unselC forState:UIControlStateNormal];
                if ([button20 isSelected])
                    [button20 setBackgroundImage:selD forState:UIControlStateNormal];
                break;
        }
        [self setQuestionPoints:4 andChoices:sender.tag-16];
    }
}

//this function will extract values from instance of DataStorageClass with adlDomain properties
-(void) setQuestionPoints:(int)qIndex andChoices:(int)cIndex{
    
    [_myDataStorage setADLDomain];
    switch (qIndex) {
        case 0:
            if(cIndex == 1)
                _Q1 = 0.0f;
            else if(cIndex == 2)
                _Q1 = [[_myDataStorage.adlDomain objectAtIndex:qIndex] B];
            else if(cIndex == 3)
                _Q1 = [[_myDataStorage.adlDomain objectAtIndex:qIndex] C];
            else 
                _Q1 = [[_myDataStorage.adlDomain objectAtIndex:qIndex] D];
            break;
        case 1:
            if(cIndex == 1)
                _Q2 =  0.0f;
            else if(cIndex == 2)
                _Q2 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] B];
            else if(cIndex == 3)
                _Q2 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] C];
            else
                _Q2 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] D];
            break;
        case 2:
            if(cIndex == 1)
                _Q3 =  0.0f;
            else if(cIndex == 2)
                _Q3 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] B];
            else if(cIndex == 3)
                _Q3 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] C];
            else
                _Q3 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] D];
            break;
        case 3:
            if(cIndex == 1)
                _Q4 =  0.0f;
            else if(cIndex == 2)
                _Q4 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] B];
            else if(cIndex == 3)
                _Q4 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] C];
            else
                _Q4 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] D];
            break;
        case 4:
            if(cIndex == 1)
                _Q5 =  0.0f;
            else if(cIndex == 2)
                _Q5 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] B];
            else if(cIndex == 3)
                _Q5 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] C];
            else
                _Q5 =  [[_myDataStorage.adlDomain objectAtIndex:qIndex] D];
            break;
    }

    _totalResult = _Q1 + _Q2 + _Q3 + _Q4 + _Q5;
    [self determineCategoryAndAmount];
}

//this function will determine the funding amount and catergory based from assessed needs
-(void)determineCategoryAndAmount{
    int i;
    NSString *myCategoryLevel;
    float myFundAmount;
    
    //this will ensure that the array will be populated
    [self.myDataStorage setADLFunding];

    for (i=0; i < [self.myDataStorage.adlFunding count]; i++)
    {
        if (_totalResult >= ((FundingClass*)[self.myDataStorage.adlFunding objectAtIndex:i]).rangeValue)
        {
            myCategoryLevel = [(FundingClass*)[self.myDataStorage.adlFunding objectAtIndex:i] myCategory];
            myFundAmount = ((FundingClass*)[self.myDataStorage.adlFunding objectAtIndex:i]).myFunding;
        }
    }
    NSLog(@"ADL Amount:%.2f",myFundAmount);
    
    rateLabel.text = [[NSString stringWithFormat:@"%c",'$'] stringByAppendingFormat:@"%.2f",myFundAmount];
    categoryLabel.text = myCategoryLevel;
    [[DataAPI sharedData] setMyAdlRate:myFundAmount];
    
    NSLog(@"ADL Category: %@",myCategoryLevel);
    NSLog(@"ADL Total Points: %.2f",_totalResult);
    NSLog(@"ADL Funding: $%.2f",[[DataAPI sharedData] getMyAdlRate]);
    printf("\n");

}

@end
