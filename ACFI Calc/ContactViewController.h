//
//  ContactViewController.h
//  fgawesgews
//
//  Created by V.NET on 10/8/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ContactViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (nonatomic,strong) IBOutlet UITextView *myMail;

-(void)done:(UIButton*)sender;
-(void)tapMailViewController;
@end
