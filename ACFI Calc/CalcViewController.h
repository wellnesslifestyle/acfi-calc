//
//  NewRateController.h
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdlViewController.h"
#import "CurrentCalcVC.h"
#import "BehViewController.h"
#import "ChcViewController.h"

@protocol CalcViewControllerDelegate;


@interface CalcViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) UITableView *myCustomTable;
@property (nonatomic,weak) id<CalcViewControllerDelegate> delegate;

-(void) didTapResetButton;
-(IBAction)tapBackButton:(UIButton*)sender;
-(void)buildTableRowAtIndexSection:(int)rowsInSection atIndexPath:(int)indexPath atRowCell:(UITableViewCell*)rowCell;
-(void)computeSummaryResult;
-(IBAction)tapResetActionSheet:(UIButton*)sender;

@end

@protocol CalcViewControllerDelegate <NSObject>

@required
-(void)CalcViewController:(CalcViewController*)calcVC updateColor:(UIColor*)myColor;

@end