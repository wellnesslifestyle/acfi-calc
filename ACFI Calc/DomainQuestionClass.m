//
//  ADL_BEH_RowQuestion.m
//  fgawesgews
//
//  Created by V.NET on 9/23/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "DomainQuestionClass.h"

@implementation DomainQuestionClass

-(id)initWithValueA:(float)a andB:(float)b andC:(float)c andD:(float)d
{
    if(self=[super init]){
        _A = a;
        _B = b;
        _C = c;
        _D = d;
    }
    return self;
}


@end
