//
//  ChcViewController.m
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "ChcViewController.h"
#import "CalcViewController.h"
#import "DataStorageClass.h"
#import "FundingClass.h"
#import "DataAPI.h"

@interface ChcViewController ()
{
    IBOutlet UIImageView *buttonHolder;
    IBOutlet UIImageView *displayHolder;
    IBOutlet UILabel *rateLabel, *categoryLabel;
    
    //Medication
    IBOutlet UIButton *medA;
    IBOutlet UIButton *medB;
    IBOutlet UIButton *medC;
    IBOutlet UIButton *medD;
    
    //CHC
    IBOutlet UIButton *chcA;
    IBOutlet UIButton *chcB;
    IBOutlet UIButton *chcC;
    IBOutlet UIButton *chcD;

    UIImage *selA,*selB,*selC,*selD,*unselA,*unselB,*unselC,*unselD;
    
   
}

@property (assign,nonatomic) float amountFunding;
@property (assign,nonatomic) int totalResult;
@property (nonatomic,strong) NSString *chcPath;
@property (nonatomic,strong) NSDictionary *chcPlist;
@end

@implementation ChcViewController
@synthesize chcPath = chcPath;
@synthesize chcPlist = chcPlist;

- (id)init
{
    //this will check if the os version is 6 and about
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        //If it is at least 6.0, it loads the nib with Auto Layout enabled.
        self = [super initWithNibName:@"ChcViewController" bundle:nil];
    }
    else
    {
        NSLog(@"version 5 to 5.9");
        self = [super initWithNibName:@"ChcViewController-legacy" bundle:nil];
    }

    
    if (self) {
        self.title = @"CHC";
        
        selA = [UIImage imageNamed:@"selectA"];
        selB = [UIImage imageNamed:@"selectB"];
        selC = [UIImage imageNamed:@"selectC"];
        selD = [UIImage imageNamed:@"selectD"];
        unselA = [UIImage imageNamed:@"unselectA"];
        unselB = [UIImage imageNamed:@"unselectB"];
        unselC = [UIImage imageNamed:@"unselectC"];
        unselD = [UIImage imageNamed:@"unselectD"];
        chcPath = [[NSBundle mainBundle] pathForResource:@"CHC_Domain" ofType:@"plist"];
        chcPlist = [NSDictionary dictionaryWithContentsOfFile:chcPath];
        self.myData = [[DataStorageClass alloc] init];
    }
    return self;
}



#pragma mark - view controller life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self customizedMyInterface];
}


#pragma mark - memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - private function for ui customization
-(void)customizedMyInterface{
    
    //set the page background color
    if (IS_IPHONE5)
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor-568h"]];
    else
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor.png"]];
    
    //set button state color
    UIImageView *selected = [[UIImageView alloc] init];
    selected.image = [[UIImage imageNamed:@"button_Selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    UIImageView *unSelected = [[UIImageView alloc] init];
    unSelected.image = [[UIImage imageNamed:@"button_Unselected"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    
    
    //set the initial of the buttons to nil
    chcA.selected = medA.selected = YES;
    if ([chcA isSelected] && [medA isSelected]) {
        [chcA setBackgroundImage:selA forState:UIControlStateNormal];
        [medA setBackgroundImage:selA forState:UIControlStateNormal];
        if (chcA.selected == YES && medA.selected == YES){
            _totalResult = 0;
        }
    }
    [chcB setBackgroundImage:unselB forState:UIControlStateNormal];
    [chcC setBackgroundImage:unselC forState:UIControlStateNormal];
    [chcD setBackgroundImage:unselD forState:UIControlStateNormal];
    [medB setBackgroundImage:unselB forState:UIControlStateNormal];
    [medC setBackgroundImage:unselC forState:UIControlStateNormal];
    [medD setBackgroundImage:unselD forState:UIControlStateNormal];

    
    //set screen display and button holder
    [displayHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"holder"]]];
    [buttonHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"buttonHolderTwo"]]];

    

    
//    self.navigationItem.title = @"New ACFI Rate";
    
    //customized my left bar button
//    UIImage *buttonSelected = [UIImage imageNamed:@"bar_left_buttonSelected"];
//    UIImage *buttonUnselected = [UIImage imageNamed:@"bar_left_buttonUnselected"];
//    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [myCustomButton setBackgroundImage:buttonSelected forState:UIControlStateHighlighted];
//    [myCustomButton setBackgroundImage:buttonUnselected forState:UIControlStateNormal];
//    myCustomButton.frame = CGRectMake(0.0f, 0.0f,buttonUnselected.size.width,buttonUnselected.size.height);
//    [myCustomButton addTarget:self action:@selector(goToPrevVC:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *myBackButton = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
//    self.navigationItem.leftBarButtonItem = myBackButton;
    
    //customized my right bar button
//    UIImage *leftButtonUnsel = [UIImage imageNamed:@"bar_button_exitUnselected"];
//    UIImage *leftButtonSel = [UIImage imageNamed:@"bar_button_exitSelected"];
//    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [doneButton setBackgroundImage:leftButtonUnsel forState:UIControlStateNormal];
//    [doneButton setBackgroundImage:leftButtonSel forState:UIControlStateHighlighted];
//    doneButton.frame = CGRectMake(0.0f, 0.0f,leftButtonUnsel.size.width,leftButtonUnsel.size.height);
//    [doneButton addTarget:self
//                        action:@selector(goToCalcVC:)
//              forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *modalBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
//	self.navigationItem.rightBarButtonItem = modalBarButtonItem;
}


//-(IBAction)goToPrevVC:(UIButton*)sender
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}

-(IBAction)goToCalcVC:(UIButton*)sender{
    //this will pop all the view controlles from 4 down to 2 except 1 which is the top of the root VC
//    [UIView animateWithDuration:0.80f
//                     animations:^{
//                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
//                     }];
//    CalcViewController *calcVC = [[CalcViewController alloc] init];
    [UIView animateWithDuration: 0.5f //.8 second fade duration
                     animations:^{
                         self.view.alpha = 0.0; //the opacity value
                     }
                     completion:^(BOOL finished) {
                         [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:NO];
//                         [self.navigationController popToRootViewControllerAnimated:NO];
                     }];
}



-(IBAction)tapButton:(UIButton *)button
{
    
    switch (button.tag)
    {
        case 1: // expression for the medication
            medA.selected = YES;
            medB.selected = medC.selected = medD.selected = NO;
            [medB setBackgroundImage:unselB forState:UIControlStateNormal];
            [medC setBackgroundImage:unselC forState:UIControlStateNormal];
            [medD setBackgroundImage:unselD forState:UIControlStateNormal];
            if([medA isSelected])
            {
                [medA setBackgroundImage:selA forState:UIControlStateNormal];
                if (chcA.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"0"] objectForKey:@"A"] integerValue];
                else if(chcB.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"0"] objectForKey:@"B"] integerValue];
                else if(chcC.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"0"] objectForKey:@"C"] integerValue];
                else if(chcD.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"0"] objectForKey:@"D"] integerValue];
            }
            break;
            
        case 2:
            medB.selected = YES;
            medA.selected = medC.selected = medD.selected = NO;
            [medA setBackgroundImage:unselA forState:UIControlStateNormal];
            [medC setBackgroundImage:unselC forState:UIControlStateNormal];
            [medD setBackgroundImage:unselD forState:UIControlStateNormal];
            if([medB isSelected]){
                [medB setBackgroundImage:selB forState:UIControlStateNormal];
                if (chcA.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"1"] objectForKey:@"A"] integerValue];

                else if(chcB.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"1"] objectForKey:@"B"] integerValue];
                else if(chcC.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"1"] objectForKey:@"C"] integerValue];
                else if(chcD.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"1"] objectForKey:@"D"] integerValue];
            }
            break;
        case 3:
            medC.selected = YES;
            medA.selected = medB.selected = medD.selected = NO;
            [medA setBackgroundImage:unselA forState:UIControlStateNormal];
            [medB setBackgroundImage:unselB forState:UIControlStateNormal];
            [medD setBackgroundImage:unselD forState:UIControlStateNormal];
            if([medC isSelected]){
                [medC setBackgroundImage:selC forState:UIControlStateNormal];
                if (chcA.selected == YES)
                     self.totalResult = [[[chcPlist objectForKey:@"2"] objectForKey:@"A"] integerValue];
                else if(chcB.selected == YES)
                     self.totalResult = [[[chcPlist objectForKey:@"2"] objectForKey:@"B"] integerValue];
                else if(chcC.selected == YES)
                     self.totalResult = [[[chcPlist objectForKey:@"2"] objectForKey:@"C"] integerValue];
                else if(chcD.selected == YES)
                     self.totalResult = [[[chcPlist objectForKey:@"2"] objectForKey:@"D"] integerValue];
            }
            break;
        case 4:
            medD.selected = YES;
            medA.selected = medB.selected = medC.selected = NO;
            [medA setBackgroundImage:unselA forState:UIControlStateNormal];
            [medB setBackgroundImage:unselB forState:UIControlStateNormal];
            [medC setBackgroundImage:unselC forState:UIControlStateNormal];
            if ([medD isSelected]){
                [medD setBackgroundImage:selD forState:UIControlStateNormal];
                if (chcA.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"3"] objectForKey:@"A"] integerValue];
                else if(chcB.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"3"] objectForKey:@"B"] integerValue];
                else if(chcC.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"3"] objectForKey:@"C"] integerValue];
                else if(chcD.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"3"] objectForKey:@"D"] integerValue];
            }
            break;
        case 5: // expression for the CHC
            chcA.selected = YES;
            chcB.selected = chcC.selected = chcD.selected = NO;
            [chcB setBackgroundImage:unselB forState:UIControlStateNormal];
            [chcC setBackgroundImage:unselC forState:UIControlStateNormal];
            [chcD setBackgroundImage:unselD forState:UIControlStateNormal];
            if([chcA isSelected]){
                [chcA setBackgroundImage:selA forState:UIControlStateNormal];
                if (medA.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"0"] objectForKey:@"A"] integerValue];
                else if(medB.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"1"] objectForKey:@"A"] integerValue];
                else if(medC.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"2"] objectForKey:@"A"] integerValue];
                else if(medD.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"3"] objectForKey:@"A"] integerValue];
            }
            break;
        case 6:
            chcB.selected = YES;
            chcA.selected = chcC.selected = chcD.selected = NO;
            [chcA setBackgroundImage:unselA forState:UIControlStateNormal];
            [chcC setBackgroundImage:unselC forState:UIControlStateNormal];
            [chcD setBackgroundImage:unselD forState:UIControlStateNormal];
            if([chcB isSelected]){
                [chcB setBackgroundImage:selB forState:UIControlStateNormal];
                if (medA.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"0"] objectForKey:@"B"] integerValue];
                else if(medB.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"1"] objectForKey:@"B"] integerValue];
                else if(medC.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"2"] objectForKey:@"B"] integerValue];
                else if(medD.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"3"] objectForKey:@"B"] integerValue];
            }
            break;
        case 7:
            chcC.selected = YES;
            chcA.selected = chcB.selected = chcD.selected = NO;
            [chcA setBackgroundImage:unselA forState:UIControlStateNormal];
            [chcB setBackgroundImage:unselB forState:UIControlStateNormal];
            [chcD setBackgroundImage:unselD forState:UIControlStateNormal];
            if([chcC isSelected]){
                [chcC setBackgroundImage:selC forState:UIControlStateNormal];
                if (medA.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"0"] objectForKey:@"C"] integerValue];
                else if(medB.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"1"] objectForKey:@"C"] integerValue];
                else if(medC.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"2"] objectForKey:@"C"] integerValue];
                else if(medD.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"3"] objectForKey:@"C"] integerValue];
            }
            break;
        case 8:
            chcD.selected = YES;
            chcA.selected = chcB.selected = chcC.selected = NO;
            [chcA setBackgroundImage:unselA forState:UIControlStateNormal];
            [chcB setBackgroundImage:unselB forState:UIControlStateNormal];
            [chcC setBackgroundImage:unselC forState:UIControlStateNormal];
            if([chcD isSelected]){
                [chcD setBackgroundImage:selD forState:UIControlStateNormal];
                if ([medA isSelected])
                    self.totalResult = [[[chcPlist objectForKey:@"0"] objectForKey:@"D"] integerValue];
                else if(medB.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"1"] objectForKey:@"D"] integerValue];
                else if(medC.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"2"] objectForKey:@"D"] integerValue];
                else if(medD.selected == YES)
                    self.totalResult = [[[chcPlist objectForKey:@"3"] objectForKey:@"D"] integerValue];
            }
            break;
    }
    
    [self determineCategoryAndAmount];
}


-(void)determineCategoryAndAmount{
    int i;
    NSString *myCategoryLevel;
    float myFundAmount;
    
    
    [self.myData setCHCFunding];
    for (i=0; i < [_myData.chcFunding count] && _totalResult != ((FundingClass*)[_myData.chcFunding objectAtIndex:i]).rangeValue ; i++);
    myCategoryLevel = [(FundingClass*)[self.myData.chcFunding objectAtIndex:i] myCategory];
    myFundAmount = ((FundingClass*)[self.myData.chcFunding objectAtIndex:i]).myFunding;
        
    

    rateLabel.text = [[NSString stringWithFormat:@"%c",'$'] stringByAppendingFormat:@"%.2f",myFundAmount];
    categoryLabel.text = myCategoryLevel;
    [[DataAPI sharedData] setMyChcRate:myFundAmount];

    NSLog(@"CHC Category: %@",myCategoryLevel);
    NSLog(@"CHC Total Points: %d",_totalResult);
    NSLog(@"CHC Funding: $%.2f",[[DataAPI sharedData] getMyChcRate]);
    printf("\n");
}



@end
