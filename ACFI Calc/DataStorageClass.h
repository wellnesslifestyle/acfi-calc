//
//  DataStorageClass.h
//  fgawesgews
//
//  Created by V.NET on 9/24/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataStorageClass : NSObject



//@property (retain,nonatomic) NSMutableArray *rateTable;

@property (strong,nonatomic) NSMutableArray *adlDomain;
@property (strong,nonatomic) NSMutableArray *behDomain;
//@property (strong,nonatomic) NSMutableArray *chcDomain;

@property (strong,nonatomic) NSMutableArray *adlFunding;
@property (strong,nonatomic) NSMutableArray *behFunding;
@property (strong,nonatomic) NSMutableArray *chcFunding;

//-(void)setRate_Table;

-(void)setADLDomain;
-(void)setBEHDomain;
//-(void)setCHCDomain;

-(void)setADLFunding;
-(void)setBEHFunding;
-(void)setCHCFunding;

@end
