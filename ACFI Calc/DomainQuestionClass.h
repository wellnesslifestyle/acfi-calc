//
//  ADL_BEH_RowQuestion.h
//  fgawesgews
//
//  Created by V.NET on 9/23/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DomainQuestionClass : NSObject

@property (nonatomic,assign) float A,B,C,D;

-(id)initWithValueA:(float)a andB:(float)b andC:(float)c andD:(float)d;

@end
