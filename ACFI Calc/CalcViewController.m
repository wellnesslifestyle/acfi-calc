//
//  NewRateController.m
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "CalcViewController.h"
#import "CellBackgroundView.h"
#import "SummaryViewController.h"
#import "DataAPI.h"
#import "SegmentedVC.h"

#define FIRST_SECTION 0
#define SECOND_SECTION 1
#define THIRD_SECTION 2

@interface CalcViewController ()
@property (assign,nonatomic) float myCurrentRate,myAdlRate,myBehRate,myChcRate,myNewRate,myDailyRate,annualChange;
@property (strong,nonatomic) NSArray *imageList;
@property (strong,nonatomic) UIBarButtonItem *rightButtonItem;
@property (strong,nonatomic) UIButton *rightButton;

-(void)didTapZeroResult;
-(void)didTapResetButton;
@end



@implementation CalcViewController
@synthesize rightButtonItem = rightButtonItem,rightButton = rightButton;


-(id)init{
    if(self=[super init]){
        _myCurrentRate = _myAdlRate = _myBehRate = _myChcRate = 0.0f;
    }
    return self;
}


#pragma mark - private function
//private function that customize UI this will be called in viewDidload()
-(void) customizeInterfaceFunction{
    
    //sets my navigation bar title
    self.navigationItem.title = @"ACFI Calculator";
    
    //programmatically create a table view
    _myCustomTable = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];

    UIImageView *tableImageBg;// = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgColor"]];
    
    
    if (IS_IPHONE5){
        tableImageBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgColor-568h"]];
    }
    else
    {
        tableImageBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgColor"]];
    }
    
    
    [tableImageBg setFrame:self.myCustomTable.frame];
    [self.myCustomTable setBackgroundView:tableImageBg];
    //    _myCustomTable.backgroundView = nil;
    [_myCustomTable setScrollEnabled:NO];
//    _myCustomTable.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor"]];
    [_myCustomTable setDataSource:self];
    [_myCustomTable setDelegate:self];
    [self.view addSubview:_myCustomTable];
    
    
    //shows my navigation bar on this page
    //    [[[self navigationController] navigationBar] setHidden:NO];
    
    
    //sets the left bar button
    UIImage *buttonSelected = [UIImage imageNamed:@"bar_left_buttonSelected"];
    UIImage *buttonUnselected = [UIImage imageNamed:@"bar_left_buttonUnselected"];
    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [myCustomButton setBackgroundImage:buttonSelected forState:UIControlStateHighlighted];
    [myCustomButton setBackgroundImage:buttonUnselected forState:UIControlStateNormal];
    myCustomButton.frame = CGRectMake(0.0f, 0.0f,buttonUnselected.size.width,buttonUnselected.size.height);
    [myCustomButton addTarget:self action:@selector(tapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *myDoneButton = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
    self.navigationItem.leftBarButtonItem = myDoneButton;
    
    
    //sets navigation bar right button
    UIImage *gearButtonUnsel = [UIImage imageNamed:@"bar_button_gearUnsel"];
    UIImage *gearButtonSel = [UIImage imageNamed:@"bar_button_gearSel"];
    rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:gearButtonUnsel forState:UIControlStateNormal];
    [rightButton setImage:gearButtonSel forState:UIControlStateHighlighted];
    rightButton.frame = CGRectMake(0.0f, 0.0f, gearButtonUnsel.size.width,gearButtonUnsel.size.height);
    [rightButton addTarget:self action:@selector(tapResetActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightButtonItem;

    

}


-(void)tapBackButton:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - my action sheet functions
//this function will be called by the navigation bar right item
-(IBAction)tapResetActionSheet:(UIButton*)sender
{
    if(_myCurrentRate > 0.0f || _myNewRate > 0.0f)
    {
        UIActionSheet *styleAlert = [[UIActionSheet alloc] initWithTitle:@"This will reset your entries that could be use for the summary."
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"Reset ACFI Entries",nil];
	
        // use the same style as the nav bar
        styleAlert.actionSheetStyle = (UIActionSheetStyle)self.navigationController.navigationBar.barStyle;
        
        //will pop up the action sheet window
        [styleAlert showFromBarButtonItem:rightButtonItem animated:YES];
    }
    else
    {
        [self didTapZeroResult];
        NSLog(@"No entries");
    }
}

#pragma mark - action sheet delegate
//action sheet delegate function
- (void)actionSheet:(UIActionSheet *)modalView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self didTapResetButton];
            break;
        default:
            break;
    }
}



#pragma mark - alert view delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        NSLog(@"Exit Alert View");
}

#pragma mark - my alert view functions
//alert view pop over window
-(void)didTapResetButton{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"ACFI rate resetted" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
    [[DataAPI sharedData] resetCurrent:0.0f andADL:0.0f andBeh:0.0f andChc:0.0f];
    [self viewWillAppear:YES];
    [alertView show];
}

-(void)didTapZeroResult{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Zero Entries" message:@"Must enter ACFI rate." delegate:self cancelButtonTitle:@"Exit" otherButtonTitles:nil,nil];
//    [self viewWillAppear:YES];
    [alertView show];
}

#pragma mark - view controller presentation

//this function notifies the view controller that its view is about to be added to a view hierarchy.
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    

    //this value exists for the lifetime of the app
    self.myCurrentRate = [[DataAPI sharedData] getMyCurrentRate];
    self.myAdlRate = [[DataAPI sharedData] getMyAdlRate];
    self.myBehRate = [[DataAPI sharedData] getMyBehRate];
    self.myChcRate = [[DataAPI sharedData] getMyChcRate];
    [self computeSummaryResult];
}



#pragma mark -
//this function will compute the current and new acfi rate
-(void)computeSummaryResult{
    _myNewRate = _myAdlRate + _myBehRate + _myChcRate;
    self.myDailyRate = _myNewRate - _myCurrentRate;
    _annualChange = self.myDailyRate * 365;
    [_myCustomTable reloadData];
}

#pragma mark - view controller life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self customizeInterfaceFunction];
}

#pragma mark - memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark
// Build my customized table cells for the parameter cell index
-(void)buildTableRowAtIndexSection:(int)rowsInSection atIndexPath:(int)indexPath atRowCell:(UITableViewCell*)rowCell{
    //my customized table cell view
    
    CellBackgroundView *myTableCell = [[CellBackgroundView alloc] init];
    rowCell.backgroundView = myTableCell;
    
    
    myTableCell.backgroundColor = [UIColor clearColor];
    myTableCell.borderColor = self.myCustomTable.separatorColor;
    //    backgroundView.fillColor = [UIColor colorWithWhite:137/255. alpha:1.0];
    
    //set the cell background color
    myTableCell.fillColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"table_cell_unselected_color"]];
    
    if(rowsInSection == 1)
    {
        myTableCell.position = CellPositionSingle;
    }
    else
    {
        if (indexPath == 0){
            myTableCell.position = CellPositionTop;
        }else if(indexPath == rowsInSection - 1){
            myTableCell.position = CellPositionBottom;
        }
        else{
            myTableCell.position = CellPositionMiddle;
        }
    }
}



#pragma mark - tableview datasource
//This dunction determines the number of sections within this table view
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}


//this function determines the number of rows for the argument section number
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case FIRST_SECTION:
            return 1;
        case SECOND_SECTION:
            return 1;
        case THIRD_SECTION:
            return 1;
        default:
            return 0;
    }
}


//Create or Access and return appropriate cell identified by the argument indexPath, that is Section number and Row number combination
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Define an identifier we can associate with table cells. Used with cell reuse as follows.
    NSString *cellID = @"tableCellID";
    
    // Ask for a cached cell that's been moved off the screen that we can therefore repurpose for a new cell coming onto the screen.
    UITableViewCell *rowCell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    
    // If no cached cells are available, create one. Depending on your table row height, we only need to create enough to fill one screen.
    // After that, the above call will start working to give us the cell that got scrolled off the screen.
    if(rowCell == nil){
        rowCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
        // Right arrow-looking indicator on the right side of the table view cell
        [rowCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        //Bolded text on the left side of the table view cell
        [rowCell.textLabel setBackgroundColor:[UIColor clearColor]];
        [rowCell.textLabel setOpaque:NO];
        [rowCell.textLabel setTextColor:[UIColor blackColor]];
        
        //text on the right side is a label that has text and is right-aligned.
        [rowCell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [rowCell.detailTextLabel setOpaque:NO];
        [rowCell.detailTextLabel setTextColor:[UIColor grayColor]];
        
    }
    
    //this is the first section of the table
    switch (indexPath.section) {
        case FIRST_SECTION:
            if(indexPath.row == 0){ //first section with one row
                rowCell.textLabel.text = @"Current ACFI Rate:";
                rowCell.detailTextLabel.text = [NSString stringWithFormat:@"$%.2f",_myCurrentRate];
            }
            break;
        case SECOND_SECTION:
            if (indexPath.row == 0)
            {
                rowCell.textLabel.text = @"New ACFI Rate:";
                rowCell.detailTextLabel.text = [NSString stringWithFormat:@"$%.2f",_myNewRate];
            }
            break;
        case THIRD_SECTION:
            if (indexPath.row == 0)//third section of the table with one row
            {
                rowCell.textLabel.text = @"Summary";
                rowCell.detailTextLabel.text = @"ACFI Rate Summary";
            }
        default:
            break;
            
    }
    
    //pass the number of cells at that specific section to be customized
    int numOfCells = [self tableView:_myCustomTable numberOfRowsInSection:indexPath.section];
    [self buildTableRowAtIndexSection:numOfCells atIndexPath:indexPath.row atRowCell:rowCell];
    
    return rowCell;
}


#pragma mark - tableview delegate
//This will call the cell touch handler and pass the associated view controller with the argument section/row
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CurrentCalcVC *currateACFI = [[CurrentCalcVC alloc] init];
//    AdlViewController *adlVC = [[AdlViewController alloc] init];
    SegmentedVC *segmentVC = [[SegmentedVC alloc] init];
    
    
    SummaryViewController *summaryVC = [[SummaryViewController alloc] initWithCurrent:_myCurrentRate andNewRate:self.myNewRate andRateChange:self.myDailyRate andAnnualChange:_annualChange];;
    
    
    
    
    //check if the navigation controller exists in view controller
    if (self.navigationController != nil){
        switch (indexPath.section){
            case FIRST_SECTION:
                if (indexPath.row == 0){
//                    self.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:currateACFI animated:YES];
//                    self.hidesBottomBarWhenPushed = NO;
                }
                break;
            case SECOND_SECTION:
                if(indexPath.row == 0){
//                    self.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:segmentVC animated:YES];
//                    self.hidesBottomBarWhenPushed = NO;
                }
                break;
            case THIRD_SECTION:
                if (indexPath.row == 0){
                    
                    [self.navigationController pushViewController:summaryVC animated:YES];
                }
                break;
            default:
                break;
        }
    }
}

@end
