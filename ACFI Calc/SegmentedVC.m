//
//  SegmentedVC.m
//  ACFI Calc
//
//  Created by V.NET on 12/2/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "SegmentedVC.h"

#import "AdlViewController.h"
#import "BehViewController.h"
#import "ChcViewController.h"

#import "NSArray+PerformSelector.h"

#import "SegmentsController.h"

@interface SegmentedVC ()

@property (nonatomic, strong) SegmentsController *segmentsController;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong) UINavigationController *navController;

- (NSArray *)segmentViewControllers;
- (void)selectedFirstSegment;

@end

@implementation SegmentedVC

- (id)init
{
    self = [super init];
    if (self) {
        self.navController = [[UINavigationController alloc] init];
    }
    return self;
}

- (NSArray *)segmentViewControllers {
    AdlViewController *adl = [[AdlViewController alloc] init];
    BehViewController *beh = [[BehViewController alloc] init];
    ChcViewController *chc = [[ChcViewController alloc] init];
    
    NSArray * viewControllers = [NSArray arrayWithObjects:adl,beh,chc,nil];
    return viewControllers;
}

- (void)selectedFirstSegment {
    //sets the view controller to be view first in the segmented tab
    self.segmentedControl.selectedSegmentIndex = 0;
    [self.segmentsController indexDidChangeForSegmentedControl:self.segmentedControl];
}


-(IBAction)done:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar_bg"] forBarMetrics:UIBarMetricsDefault];
    
    NSArray * viewControllers = [self segmentViewControllers];
    
    self.navigationItem.title = @"New ACFI Rate";
    
    // Do any additional setup after loading the view from its nib.
    UIImage *buttonSelected = [UIImage imageNamed:@"bar_left_buttonSelected"];
    UIImage *buttonUnselected = [UIImage imageNamed:@"bar_left_buttonUnselected"];
    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [myCustomButton setBackgroundImage:buttonSelected forState:UIControlStateHighlighted];
    [myCustomButton setBackgroundImage:buttonUnselected forState:UIControlStateNormal];
    myCustomButton.frame = CGRectMake(0.0f, 0.0f,buttonUnselected.size.width,buttonUnselected.size.height);
    [myCustomButton addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *myDoneButton = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
    self.navigationItem.leftBarButtonItem = myDoneButton;
    
    
    self.segmentsController = [[SegmentsController alloc] initWithNavigationController:self.navController viewControllers:viewControllers];
    
    
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:[viewControllers arrayByPerformingSelector:@selector(title)]];
    
    self.segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    self.segmentedControl.frame = CGRectMake(0, 0, 210.0f, 38.0f);
    
    [self.segmentedControl addTarget:self.segmentsController
                              action:@selector(indexDidChangeForSegmentedControl:)
                    forControlEvents:UIControlEventValueChanged];
    
    [self selectedFirstSegment];
    
    //    [self.navigationController.view addSubview:self.navController.view];
    [self.view addSubview:self.navController.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

@end
