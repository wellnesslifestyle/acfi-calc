//
//  UIPopoverController+IPhonePopover.h
//  fgawesgews
//
//  Created by V.NET on 9/24/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPopoverController (IPhonePopover)

+(BOOL)_popoversDisabled;
@end
