//
//  SegmentsController.m
//  ACFI Calc
//
//  Created by V.NET on 12/1/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "SegmentsController.h"
#import "SegmentedVC.h"

@implementation SegmentsController

- (id)initWithNavigationController:(UINavigationController *)aNavigationController
                   viewControllers:(NSArray *)theViewControllers
//                      navBarButton:(UIBarButtonItem*)backButton
{
    if (self = [super init]) {
        _navigationController   = aNavigationController;
        _viewControllers = theViewControllers;
//        _navBackButton = backButton;
    }
    return self;
}

- (void)indexDidChangeForSegmentedControl:(UISegmentedControl *)aSegmentedControl {
    NSUInteger index = aSegmentedControl.selectedSegmentIndex;
    UIViewController *incomingViewController = [self.viewControllers objectAtIndex:index];
    
    NSArray * theViewControllers = [NSArray arrayWithObject:incomingViewController];
    
    [self.navigationController setViewControllers:theViewControllers animated:NO];
    
    incomingViewController.navigationItem.titleView = aSegmentedControl;
//    incomingViewController.navigationItem.leftBarButtonItem = _navBackButton;
    
//    Do any additional setup after loading the view from its nib.
//    UIImage *buttonSelected = [UIImage imageNamed:@"bar_left_buttonSelected"];
//    UIImage *buttonUnselected = [UIImage imageNamed:@"bar_left_buttonUnselected"];
//    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [myCustomButton setBackgroundImage:buttonSelected forState:UIControlStateHighlighted];
//    [myCustomButton setBackgroundImage:buttonUnselected forState:UIControlStateNormal];
//    myCustomButton.frame = CGRectMake(0.0f, 0.0f,buttonUnselected.size.width,buttonUnselected.size.height);
//    [myCustomButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *myDoneButton = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
//    incomingViewController.navigationItem.leftBarButtonItem = myDoneButton;
  
}


@end
