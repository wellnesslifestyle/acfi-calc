//
//  FundingClass.h
//  fgawesgews
//
//  Created by V.NET on 9/24/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FundingClass : NSObject

@property (nonatomic,assign) int rangeValue;
@property (strong,nonatomic) NSString *myCategory;
@property (nonatomic,assign) float myFunding;

-(id)initWithRange:(int)range andCategory:(NSString*)category andFunding:(float)amount;

@end
