//
//  CurrentCalcVC.m
//  fgawesgews
//
//  Created by admin on 9/17/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "CurrentCalcVC.h"
#import "FundingClass.h"
#import "CalcViewController.h"
#import "DataAPI.h"
#import "ModalViewController.h"

#define NEW_RATE 1

@interface CurrentCalcVC ()
{
    IBOutlet UILabel *adlTitle;
    IBOutlet UILabel *behTitle;
    IBOutlet UILabel *chcTitle;
    
    IBOutlet UILabel *currentTitle;
    
    IBOutlet UIButton *rateButton;
    
    IBOutlet UILabel *dailyRateLabel;
}

@property (retain,nonatomic) UIImage *selectNil,*selectLow,*selectMed,*selectHigh,*rateButon;
@property (retain,nonatomic) UIImage *unselectNil,*unselectLow,*unselectMed,*unselectHigh;

@property (strong) IBOutlet UIImageView *currentImage;

//adl buttons
@property (strong,nonatomic) IBOutlet UIButton *adlA;
@property (strong,nonatomic) IBOutlet UIButton *adlB;
@property (strong,nonatomic) IBOutlet UIButton *adlC;
@property (strong,nonatomic) IBOutlet UIButton *adlD;

//beh buttons
@property (strong,nonatomic) IBOutlet UIButton *behA;
@property (strong,nonatomic) IBOutlet UIButton *behB;
@property (strong,nonatomic) IBOutlet UIButton *behC;
@property (strong,nonatomic) IBOutlet UIButton *behD;

//chc buttons
@property (strong,nonatomic) IBOutlet UIButton *chcA;
@property (strong,nonatomic) IBOutlet UIButton *chcB;
@property (strong,nonatomic) IBOutlet UIButton *chcC;
@property (strong,nonatomic) IBOutlet UIButton *chcD;


-(void)SetCurrentRate;
@end

@implementation CurrentCalcVC

@synthesize adlA = adlA, adlB = adlB, adlC = adlC, adlD = adlD;
@synthesize behA = behA, behB = behB, behC = behC, behD = behD;
@synthesize chcA = chcA, chcB = chcB, chcC = chcC, chcD = chcD;
@synthesize  selectNil = selectNil,selectLow = selectLow,selectMed = selectMed,selectHigh = selectHigh;
@synthesize unselectNil = unselectNil,unselectLow = unselectLow,unselectMed = unselectMed,unselectHigh = unselectHigh;

#pragma mark - myCustomized UI in private method
-(void)customizedInterfaceComponents{
    
    //page background
    if (IS_IPHONE5)
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor-568h"]];
    else
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor.png"]];
    
    //Navigation Bar Title
    [[self navigationItem] setTitle:@"Current ACFI Rate"];
    
    UIImage *currentRateImg = [UIImage imageNamed:@"currentRate_title"];
    UIImageView *rateTitle = [[UIImageView alloc] initWithFrame:CGRectMake(85.0f, 70.0f, 162.0f, 14.0f)];
    rateTitle.image = currentRateImg;
    [self.view addSubview:rateTitle];
    
    //set the right bar button item
    UIButton *modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
	[modalViewButton addTarget:self
                        action:@selector(modalViewAction:)
              forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *modalBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
	self.navigationItem.rightBarButtonItem = modalBarButtonItem;
    
    
    
    //Navigation Bar Title
    currentTitle.text = @"Select Current Rate";
    [currentTitle setAdjustsFontSizeToFitWidth:YES];
    
    
    //set text holder attributes
    [_currentImage setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"rateHolder"]]];
    
    //    UIImage *tabBG= [[UIImage imageNamed:@"button_color"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    //    [[UIButton appearance] setBackgroundImage:tabBG];
    
    
    
    //Domain Title color
    [adlTitle setTextColor:[UIColor whiteColor]];
    [behTitle setTextColor:[UIColor whiteColor]];
    [chcTitle setTextColor:[UIColor whiteColor]];
    
    //Domain Font Adjustment
    [adlTitle setAdjustsFontSizeToFitWidth:YES];
    [behTitle setAdjustsFontSizeToFitWidth:YES];
    [chcTitle setAdjustsFontSizeToFitWidth:YES];
    
    //Configuring the Button Attributes
    //    UIImage *selectedNil = [UIImage imageNamed:@"button_SelectedNil"];
    //    UIImage *unselNil = [UIImage imageNamed:@"button_UnselectedNil"];
    //    UIButton *nilButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [nilButton setBackgroundImage:unselNil forState:UIControlStateNormal];
    //    [nilButton setBackgroundImage:selectedNil forState:UIControlStateHighlighted];
    //    nilButton.frame = CGRectMake(0.0f, 0.0f, selectedNil.size.width, selectedNil.size.height);
    //    UIImageView *selectedLow = [UIImage imageNamed:@"string"]
    //    UIImageView *selectedMed =
    //    UIImageView *selectedHigh
    
    [adlA setBackgroundImage:[UIImage imageNamed:@"button_UnselectedNil"]  forState:UIControlStateNormal];
    [adlB setBackgroundImage:[UIImage imageNamed:@"button_UnselectedLow"] forState:UIControlStateNormal];
    [adlC setBackgroundImage:[UIImage imageNamed:@"button_UnselectedMed"] forState:UIControlStateNormal];
    [adlD setBackgroundImage:[UIImage imageNamed:@"button_UnselectedHigh"] forState:UIControlStateNormal];
    
    [behA setBackgroundImage:[UIImage imageNamed:@"button_UnselectedNil"] forState:UIControlStateNormal];
    [behB setBackgroundImage:[UIImage imageNamed:@"button_UnselectedLow"] forState:UIControlStateNormal];
    [behC setBackgroundImage:[UIImage imageNamed:@"button_UnselectedMed"] forState:UIControlStateNormal];
    [behD setBackgroundImage:[UIImage imageNamed:@"button_UnselectedHigh"] forState:UIControlStateNormal];
    
    [chcA setBackgroundImage:[UIImage imageNamed:@"button_UnselectedNil"] forState:UIControlStateNormal];
    [chcB setBackgroundImage:[UIImage imageNamed:@"button_UnselectedLow"] forState:UIControlStateNormal];
    [chcC setBackgroundImage:[UIImage imageNamed:@"button_UnselectedMed"] forState:UIControlStateNormal];
    [chcD setBackgroundImage:[UIImage imageNamed:@"button_UnselectedHigh"] forState:UIControlStateNormal];

    //sets rate button
    [rateButton setBackgroundImage:[UIImage imageNamed:@"rates_button"] forState:UIControlStateNormal];
    [rateButton setTitle:@"Select Here" forState:UIControlStateNormal];
    rateButton.titleLabel.textColor = [UIColor blackColor];
    [rateButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, -30.0f, 0.0f, 0.0f)];
    
    
    _currentRate.text = @"$ 0.00";
    
    
    //customizes my left bar button
    UIImage *buttonSelected = [UIImage imageNamed:@"bar_left_buttonSelected"];
    UIImage *buttonUnselected = [UIImage imageNamed:@"bar_left_buttonUnselected"];
    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [myCustomButton setBackgroundImage:buttonSelected forState:UIControlStateHighlighted];
    [myCustomButton setBackgroundImage:buttonUnselected forState:UIControlStateNormal];
    myCustomButton.frame = CGRectMake(0.0f, 0.0f,buttonUnselected.size.width,buttonUnselected.size.height);
    [myCustomButton addTarget:self action:@selector(returnCurrentFund:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *myDoneButton = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
    self.navigationItem.leftBarButtonItem = myDoneButton;
    
}

#pragma mark - initialization function
-(id)init{
    
    //this will check if the os version is 6 and about
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        //If it is at least 6.0, it loads the nib with Auto Layout enabled.
        self = [super initWithNibName:@"CurrentCalcVC" bundle:nil];
    }
    else
    {
        NSLog(@"version 5 to 5.9");
        self = [super initWithNibName:@"CurrentCalcVC-legacy" bundle:nil];
    }
    
    if (self) {
         _adlResult = _behResult = _chcResult = 0.0;
        _myData = [[DataStorageClass alloc] init];
        selectNil = [UIImage imageNamed:@"button_SelectedNil"];
        unselectNil = [UIImage imageNamed:@"button_UnselectedNil"];
        selectLow = [UIImage imageNamed:@"button_SelectedLow"];
        unselectLow = [UIImage imageNamed:@"button_UnselectedLow"];
        selectMed = [UIImage imageNamed:@"button_SelectedMed"];
        unselectMed = [UIImage imageNamed:@"button_UnselectedMed"];
        selectHigh = [UIImage imageNamed:@"button_SelectedHigh"];
        unselectHigh = [UIImage imageNamed:@"button_UnselectedHigh"];
    }
    
    return self;
}



- (IBAction)modalViewAction:(id)sender
{
    
    //        self.myModalViewController = [[ModalViewController alloc] initWithNibName:
    //                                      NSStringFromClass([ModalViewController class]) bundle:nil];
    ModalViewController *myModalViewController = [[ModalViewController alloc] init];
    self.hidesBottomBarWhenPushed = NO;
    [self.navigationController presentViewController:myModalViewController animated:YES completion:nil];
    
    
    printf("\n");
}

#pragma mark -
-(IBAction)returnCurrentFund:(UIButton*)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    //this will inform if the delegate class is exist;
    //    if(_delegate != nil){
    //        [self.delegate CurrentCaclViewController:self didFinishChoosingRate:_currentRateResult];
    //    }
}




#pragma mark - acfi buttons
//this function determines the rate amount in each domain
-(IBAction)tapRatesButton:(UIButton *)sender
{
   
    switch (sender.tag){
        case 1:
            adlB.selected = adlC.selected = adlD.selected = NO;
            adlA.selected = YES;
            [adlB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [adlC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            [adlD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([adlA isSelected]){
                [adlA setBackgroundImage:selectNil forState:UIControlStateNormal];
                [self computeAdlFunds:sender.tag];
            }
            break;
        case 2:
            adlA.selected = adlC.selected = adlD.selected = NO;
            adlB.selected = YES;
            [adlA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [adlC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            [adlD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([adlB isSelected]){
                [adlB setBackgroundImage:selectLow forState:UIControlStateNormal];
                [self computeAdlFunds:sender.tag];
            }
            break;
        case 3:
            adlA.selected = adlB.selected = adlD.selected = NO;
            adlC.selected = YES;
            [adlA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [adlB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [adlD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([adlC isSelected]){
                [adlC setBackgroundImage:selectMed forState:UIControlStateNormal];
                [self computeAdlFunds:sender.tag];
            }
            break;
        case 4:
            adlA.selected = adlB.selected = adlC.selected = NO;
            adlD.selected = YES;
            [adlA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [adlB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [adlC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            if ([adlD isSelected]){
                [adlD setBackgroundImage:selectHigh forState:UIControlStateNormal];
                [self computeAdlFunds:sender.tag];
            }
            break;
        case 5:
            behB.selected = behC.selected = behD.selected = NO;
            behA.selected = YES;
            [behB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [behC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            [behD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([behA isSelected]) {
                [behA setBackgroundImage:selectNil forState:UIControlStateNormal];
                [self computeBehFunds:sender.tag];
            }
            break;
        case 6:
            behB.selected = YES;
            behA.selected = behC.selected = behD.selected = NO;
            [behA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [behC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            [behD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([behB isSelected]) {
                [behB setBackgroundImage:selectLow forState:UIControlStateNormal];
                [self computeBehFunds:sender.tag];
            }
            break;
            
        case 7:
            behC.selected = YES;
            behA.selected = behB.selected = behD.selected = NO;
            [behA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [behB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [behD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([behC isSelected]) {
                [behC setBackgroundImage:selectMed forState:UIControlStateNormal];
                [self computeBehFunds:sender.tag];
            }
            break;
        case 8:
            behD.selected = YES;
            behA.selected = behB.selected = behC.selected = NO;
            [behA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [behB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [behC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            if ([behD isSelected]) {
                [behD setBackgroundImage:selectHigh forState:UIControlStateNormal];
                [self computeBehFunds:sender.tag];
            }
            break;
        case 9:
            chcA.selected = YES;
            [chcB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [chcC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            [chcD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([chcA isSelected]) {
                [chcA setBackgroundImage:selectNil forState:UIControlStateNormal];
                [self computeChcFunds:sender.tag];
            }
            break;
        case 10:
            chcB.selected = YES;
            [chcA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [chcC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            [chcD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([chcB isSelected]) {
                [chcB setBackgroundImage:selectLow forState:UIControlStateNormal];
                [self computeChcFunds:sender.tag];
            }
            break;
        case 11:
            chcC.selected = YES;
            [chcA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [chcB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [chcD setBackgroundImage:unselectHigh forState:UIControlStateNormal];
            if ([chcC isSelected]) {
                [chcC setBackgroundImage:selectMed forState:UIControlStateNormal];
                [self computeChcFunds:sender.tag];
            }
            break;
            break;
        case 12:
            chcD.selected = YES;
            [chcA setBackgroundImage:unselectNil forState:UIControlStateNormal];
            [chcB setBackgroundImage:unselectLow forState:UIControlStateNormal];
            [chcC setBackgroundImage:unselectMed forState:UIControlStateNormal];
            if ([chcD isSelected]){
                [chcD setBackgroundImage:selectHigh forState:UIControlStateNormal];
                [self computeChcFunds:sender.tag];
            }
            break;
    }
    
    NSLog(@"%@",_currentRate.text);
}




#pragma mark - ACFI Domain Calculations
//function calculates the selected button in activity daily living domain
-(void)computeAdlFunds:(int)tagVal{
    
    float fundAmount;
    NSString *category;
    int i,range = 0;
    
    [_myData setADLFunding];
    for(i=1;i <= tagVal;i++)
    {
        NSLog(@"ADL Choosen tag %d",tagVal);
        switch (i){
            case 1:
                range = 0;
                break;
            case 2:
                //adl Low
                range = ((FundingClass*)[_myData.adlFunding objectAtIndex:1]).rangeValue;
                NSLog(@"ADL Range: %d",range);
                break;
            case 3:
                //adl Med
                range = ((FundingClass*)[_myData.adlFunding objectAtIndex:2]).rangeValue;
                break;
            case 4:
                //adl High
                range = ((FundingClass*)[_myData.adlFunding objectAtIndex:3]).rangeValue;
                break;
            default:
                break;
        }
    }
    
    for (i=0; i < [_myData.adlFunding count] && range > ((FundingClass*)[_myData.adlFunding objectAtIndex:i]).rangeValue; i++);
    fundAmount = [(FundingClass*)[_myData.adlFunding objectAtIndex:i] myFunding];
    category = [(FundingClass*)[_myData.adlFunding objectAtIndex:i] myCategory];
    
    
    NSLog(@"ADL Range: %d",range);
    NSLog(@"ADL Category:%@",category);
    NSLog(@"ADL Funds: %.2f",fundAmount);
    printf("\n");
    
    _adlResult = fundAmount;
    [self getResult];
}

// function calculates the selected button in behavioral domain
-(void)computeBehFunds:(int)tagVal{
    float fundAmount;
    NSString *category;
    int range,i;
    
    [_myData setBEHFunding];
    for (i=5; i<=tagVal; i++) {
        switch (tagVal){
            case 5:
                range = 0;
                break;
            case 6:
                // beh low
                range = ((FundingClass*)[_myData.behFunding objectAtIndex:1]).rangeValue;
                break;
            case 7:
                // beh med
                range = ((FundingClass*)[_myData.behFunding objectAtIndex:2]).rangeValue;
                break;
            case 8:
                // beh high
                range = ((FundingClass*)[_myData.behFunding objectAtIndex:3]).rangeValue;
                break;
        }
    }
    
    for (i=0; i < [_myData.behFunding count] && range > ((FundingClass*)[_myData.behFunding objectAtIndex:i]).rangeValue; i++);
    fundAmount = [(FundingClass*)[_myData.behFunding objectAtIndex:i] myFunding];
    category = [(FundingClass*)[_myData.behFunding objectAtIndex:i] myCategory];
    
    
    NSLog(@"BEH Range: %d",range);
    NSLog(@"BEH Category:%@",category);
    NSLog(@"BEH Funds: %.2f",fundAmount);
    printf("\n");
    
    _behResult = fundAmount;
    [self getResult];
}


//this function will calculates the selected button in complex health care domain
-(void)computeChcFunds:(int)tagVal{
    float fundAmount;
    NSString *category;
    int i,range;
    
    [_myData setCHCFunding];
    for(i = 9; i <= tagVal;i++){
        switch (i) {
            case 9:
                range = 0;
                break;
            case 10:
                //chc low
                range = ((FundingClass*)[_myData.chcFunding objectAtIndex:1]).rangeValue;
                break;
            case 11:
                //chc med
                range = ((FundingClass*)[_myData.chcFunding objectAtIndex:2]).rangeValue;
                break;
            case 12:
                //chc high
                range = ((FundingClass*)[_myData.chcFunding objectAtIndex:3]).rangeValue;
                break;
            default:
                break;
        }
    }
    
    for (i=0; i < [_myData.chcFunding count] && range != ((FundingClass*)[_myData.chcFunding objectAtIndex:i]).rangeValue; i++);
    fundAmount = [(FundingClass*)[_myData.chcFunding objectAtIndex:i] myFunding];
    category = [(FundingClass*)[_myData.chcFunding objectAtIndex:i] myCategory];
    
    
    NSLog(@"CHC Range: %d",range);
    NSLog(@"CHC Category:%@",category);
    NSLog(@"CHC Funds: %.2f",fundAmount);
    printf("\n");
    
    _chcResult = fundAmount;
    [self getResult];
}


#pragma mark - acfi total funding
//this function will get the funding amount from the user input
-(void)getResult{
    float sum = _adlResult + _behResult + _chcResult;
    printf("\n");
    NSLog(@"Total Funds: $%.2f",sum);
    printf("\n");
    _currentRate.text = [[NSString stringWithFormat:@"%c ",'$'] stringByAppendingFormat:@"%.2f",sum];
    _currentRateResult = sum;
    [self SetCurrentRate];
}

-(void)SetCurrentRate
{
    [[DataAPI sharedData] setMyCurrentRate:self.currentRateResult];
}

#pragma  mark - my custom delegate method
//complying to my delegating object, When user choose the RCS rates
-(void)ListOfRatesViewController:(ListOfRatesViewController *)myListVC updateRateFunding:(float)myAmount andButtonLabel:(NSString *)myLabel{
    
    
    NSLog(@"amount: %.2f",myAmount);
    [rateButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 0.0f)];
    rateButton.titleLabel.text = myLabel;
    
    //    rateButton.titleLabel.textColor = myColor;
    _currentRate.text = [[NSString stringWithFormat:@"%c ",'$'] stringByAppendingFormat:@"%.2f",myAmount];
    _currentRateResult = myAmount;
    [self SetCurrentRate];
    
    //This will dismiss the popover if it is showing.
    if (_rateValuePopover) {
        [_rateValuePopover dismissPopoverAnimated:YES];
        _rateValuePopover = nil;
    }
    
}


#pragma mark - my custom popover
//popover method for my dropdown current rates
-(IBAction)chooseDropDownRates:(UIButton *)sender{
    _currentRateResult = 0.0;
    _currentRate.text = @"$ 0.00";
    
    [self customizedInterfaceComponents];
    
    if(_rateValuePicker == nil){
        _rateValuePicker = [[ListOfRatesViewController alloc] initWithStyle:UITableViewStyleGrouped];
        _rateValuePicker.delegate = self;
    }
    
    if(_rateValuePopover == nil){
        
        _rateValuePopover = [[UIPopoverController alloc] initWithContentViewController:_rateValuePicker];
        [_rateValuePopover presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else{
        //Hide the color picker popover if it is showing
        
        [_rateValuePopover dismissPopoverAnimated:YES];
        
        _rateValuePopover = nil;
    }
}

#pragma mark - view life cycle
- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self customizedInterfaceComponents];
}


#pragma mark - memory management
- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
