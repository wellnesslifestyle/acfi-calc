//
//  DataAPI.m
//  ACFI Calc
//
//  Created by V.NET on 10/14/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "DataAPI.h"


@interface DataAPI ()

@end

@implementation DataAPI
-(id)init{
    if(self=[super init]){
    }
    return self;
}

+(DataAPI *)sharedData{
    static DataAPI *sharedInstance = nil;
     //it ensures that the initialization code executes only once.
    static dispatch_once_t myPredicate;
    //using Grand Central Dispatch(GCD) to execute a block which initializes an instance of DataAPI.
    dispatch_once(&myPredicate, ^{ 
        sharedInstance = [[DataAPI alloc] init];
    });
    return sharedInstance;
}

-(void)setMyCurrentRate:(float)myRate{
    _currentAmount = myRate;
}
-(float)getMyCurrentRate{
    return _currentAmount;
}
-(void)setMyAdlRate:(float)myAdl{
    _adlAmount = myAdl;
}
-(float)getMyAdlRate{
    return _adlAmount;
}
-(void)setMyBehRate:(float)myBeh{
    _behAmount = myBeh;
}
-(float)getMyBehRate{
    return _behAmount;
}
-(void)setMyChcRate:(float)myChc{
    _chcAmount = myChc;
}
-(float)getMyChcRate{
    return _chcAmount;
}
-(void)resetCurrent:(float)cur andADL:(float)adl andBeh:(float)beh andChc:(float)chc{
    _currentAmount = cur;
    _adlAmount = adl;
    _behAmount = beh;
    _chcAmount = chc;
}
@end
