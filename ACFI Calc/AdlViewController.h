//
//  AdlViewController.h
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DataStorageClass;


//@protocol AdlDelegate;

@interface AdlViewController : UIViewController

@property (strong,nonatomic) DataStorageClass *myDataStorage;
//@property (weak,nonatomic) id<AdlDelegate> delegate;

-(IBAction) tapButtons:(UIButton*)sender;
-(IBAction)goToBehVC:(UIButton*)sender;
-(void) setQuestionPoints:(int)qIndex andChoices:(int)cIndex;
-(void)determineCategoryAndAmount;
-(IBAction)done:(id)sender;

@end
//set my protocol implementation to pass the amount to New Rate View Controller
//@protocol AdlDelegate <NSObject>
//@required
//-(void)adlViewController:(AdlViewController*)adlVC didFinishSelectingRate:(float)fundingAmount;
