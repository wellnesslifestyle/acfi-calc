//
//  AppDelegate.m
//  fgawesgews
//
//  Created by V.NET on 9/16/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - customized TabBar
-(void) customizedInterface{
    
    //set tab bar background
//    UIImage *tabBG= [[UIImage imageNamed:@"tab_indicator"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
//    [[UITabBar appearance] setBackgroundImage:tabBG];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tab_bar_bg"]];
    
    
    //sets navigation bar attributes
//    UIImage *navBarBg = [[UIImage imageNamed:@"navigation_bar_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 0, 30.0f, 0.0f)];
//    [[UINavigationBar appearance] setBackgroundImage:navBarBg forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      [UIColor darkTextColor] /*colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]*/,UITextAttributeTextColor,
//      [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0],UITextAttributeTextShadowColor,
//      [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],UITextAttributeTextShadowOffset,
//      [UIFont fontWithName:@"Arial-Bold" size:0.0],UITextAttributeFont,
//      nil]];
    
    
    //sets my status bar style
    [UIApplication sharedApplication].statusBarStyle = UIBarStyleBlackTranslucent;
    
    //sets navigation bar shadow for ios6 or later
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
//        [[UINavigationBar appearance] setShadowImage:[UIImage imageNamed:@"bar_shadow"]];
    
    
    
    UIImage *segmentSelected = [[UIImage imageNamed:@"segcontrol_sel.png"]
                                resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    UIImage *segmentUnselected = [[UIImage imageNamed:@"segcontrol_uns.png"]
                                  resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    UIImage *segmentSelectedUnselected = [UIImage imageNamed:@"segcontrol_sel-uns.png"];
    UIImage *segUnselectedSelected = [UIImage imageNamed:@"segcontrol_uns-sel.png"];
    UIImage *segmentUnselectedUnselected = [UIImage imageNamed:@"segcontrol_uns-uns.png"];
    
    
    [[UISegmentedControl appearance] setBackgroundImage:segmentUnselected
                                               forState:UIControlStateNormal
                                             barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:segmentSelected
                                               forState:UIControlStateSelected
                                             barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setDividerImage:segmentUnselectedUnselected
                                 forLeftSegmentState:UIControlStateNormal
                                   rightSegmentState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    
    [[UISegmentedControl appearance] setDividerImage:segmentSelectedUnselected
                                 forLeftSegmentState:UIControlStateSelected
                                   rightSegmentState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance]
     setDividerImage:segUnselectedSelected
     forLeftSegmentState:UIControlStateNormal
     rightSegmentState:UIControlStateSelected
     barMetrics:UIBarMetricsDefault];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor darkGrayColor],UITextAttributeTextColor,
                                               [UIColor lightGrayColor],UITextAttributeTextShadowColor,
                                               [NSValue valueWithUIOffset:UIOffsetMake(0, 1)],UITextAttributeTextShadowOffset,
                                               [UIFont fontWithName:@"Arial-Bold" size:0.0],UITextAttributeFont,
                                               nil]
                                     forState:UIControlStateNormal];

    
    
}


#pragma mark -
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.vc1 = [[HomeViewController alloc] init];
    
   
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    //myCustomize my layout
    [self customizedInterface];

//    self.vc1.tabBarItem = [[UITabBarItem alloc] init];
//    [[[self vc1] tabBarItem] setTag:1];
//    [[[self vc1] tabBarItem] setTitle:@"Home"];
//    [[[self vc1] tabBarItem] setFinishedSelectedImage:[UIImage imageNamed:@"tab_active_home"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_inactive_home"]];

    
    //set Navigation controller in home view controller
    self.navVC = [[UINavigationController alloc] initWithRootViewController:self.vc1];
    
    //Navigation controller in a tab bar controller
    UITabBarController *tabVC = [[UITabBarController alloc] init];
    
    tabVC.viewControllers = [NSArray arrayWithObjects:self.navVC,nil];
    
    //disable the tabbar interaction just for aesthetic purposes
    tabVC.tabBar.userInteractionEnabled = NO;
    
    self.window.rootViewController = tabVC;
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
