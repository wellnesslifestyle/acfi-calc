//
//  AppDelegate.h
//  fgawesgews
//
//  Created by V.NET on 9/16/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeViewController;
@class ResidentViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) HomeViewController *vc1;
@property (strong, nonatomic) UINavigationController *navVC;

@end
