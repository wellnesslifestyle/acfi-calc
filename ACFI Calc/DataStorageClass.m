//
//  DataStorageClass.m
//  fgawesgews
//
//  Created by V.NET on 9/24/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "DataStorageClass.h"
#import "CurrentRates.h"
#import "DomainQuestionClass.h"
#import "FundingClass.h"

@interface DataStorageClass ()


@end

@implementation DataStorageClass

-(id)init{
    if (self = [super init]) {
        
//        self.rateTable = [[NSMutableArray alloc] init];
        
        self.adlDomain = [NSMutableArray array];
        self.behDomain = [NSMutableArray array];
//        self.chcDomain = [NSMutableArray array];
        
        self.adlFunding = [NSMutableArray array];
        self.behFunding = [NSMutableArray array];
        self.chcFunding = [NSMutableArray array];
    }
    return self;
}


//#pragma mark - current rate values
//populate with current rate values
//-(void)setRate_Table{
// 
//    
//    NSDictionary *temp = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RateTable" ofType:@"plist"]];
//
//
//    for(int i=0;i < [temp count]; i++){
//        NSString *rateKey = [[temp objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"0"];
//        NSNumber *myFund = [[temp objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"1"];
//        float amount = [myFund floatValue];
//        CurrentRates *myRates = [[CurrentRates alloc] initWithRate:rateKey andAmount:amount];
//        [_rateTable addObject:myRates];
//    }
//}


#pragma mark - acfi domain question point system
//populate with ADL point system question
-(void)setADLDomain
{    
    NSDictionary *tempDict;
    float b,c,d;
    DomainQuestionClass *myADL;
    //Get the activity daily living data from the ADL_Domain property list.
    tempDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ADL_Domain" ofType:@"plist"]];
    
    for (int i=0; i < [tempDict count]; i++)
    {
        b = [[[tempDict objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"B"] floatValue];
        c = [[[tempDict objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"C"] floatValue];
        d = [[[tempDict objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"D"] floatValue];
        
        myADL = [[DomainQuestionClass alloc] initWithValueA:0.0 andB:b andC:c andD:d];
        [_adlDomain addObject:myADL];
    }
}

//populates with Behavioral point system question
-(void)setBEHDomain
{
    NSDictionary *tempDict;
    float b,c,d;
    DomainQuestionClass *myBEH;
    //Get the behaviour data from the BEH_Domain property list.
    tempDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"BEH_Domain" ofType:@"plist"]];
 
    for (int i=0; i < [tempDict count]; i++)
    {
        b = [[[tempDict objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"B"] floatValue];
        c = [[[tempDict objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"C"] floatValue];
        d = [[[tempDict objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"D"] floatValue];
        
        myBEH = [[DomainQuestionClass alloc] initWithValueA:0.0 andB:b andC:c andD:d];
        [_behDomain addObject:myBEH];
    }
}


#pragma mark - acfi domain funding
//populates with ADL funding from plist
-(void)setADLFunding
{
    NSDictionary *adlFundingPlist = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ADL_Funding" ofType:@"plist"]];
    
    FundingClass *adlObj;
    
    
    int myRange,i;
    NSString *myCategory;
    float myFunding;
    
    for (i=0; i < [adlFundingPlist count]; i++) {
        
        myRange = [[[adlFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"range"] intValue];
        myCategory = [[adlFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"class"];
        myFunding = [[[adlFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"funds"] floatValue];
        
        adlObj = [[FundingClass alloc] initWithRange:myRange andCategory:myCategory andFunding:myFunding];
        
        [_adlFunding addObject:adlObj];
    }
}


//populates with Behavioral funding from plist
-(void)setBEHFunding
{
    NSDictionary *behFundingPlist = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"BEH_Funding" ofType:@"plist"]];
    FundingClass *behObj;
    
    int myRange,i;
    NSString *myCategory;
    float myFunding;
    
    
    for (i=0; i < [behFundingPlist count]; i++) {
        myRange = [[[behFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"range"] intValue];
        myCategory = [[behFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"class"];
        myFunding = [[[behFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"funds"] floatValue];
        
        behObj = [[FundingClass alloc] initWithRange:myRange andCategory:myCategory andFunding:myFunding];
        [_behFunding addObject:behObj];
    }
}

//populates with Complex Health Care funding from plist
-(void)setCHCFunding
{
    NSDictionary *chcFundingPlist = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CHC_Funding" ofType:@"plist"]];
    FundingClass *chcFundingObj;
    
    int myRange,i;
    NSString *myCategory;
    float myFunding;
    
    for (i=0; i < [chcFundingPlist count]; i++) {
        
        myRange = [[[chcFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"range"] intValue];
        myCategory = [[chcFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"class"];
        myFunding = [[[chcFundingPlist objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"funds"] floatValue];
        
        chcFundingObj = [[FundingClass alloc] initWithRange:myRange andCategory:myCategory andFunding:myFunding];
        [self.chcFunding insertObject:chcFundingObj atIndex:i];
    }
}

@end
