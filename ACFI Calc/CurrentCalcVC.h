//
//  CurrentCalcVC.h
//  fgawesgews
//
//  Created by admin on 9/17/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListOfRatesViewController.h"
//#import "UIPopoverController+IPhonePopover.h"
@class ModalViewController;
//@protocol CurrentCalcVCDelegate;

@interface CurrentCalcVC : UIViewController <ListOfRatesDelegate,UIActionSheetDelegate,UIAlertViewDelegate>

@property (nonatomic,strong) UIBarButtonItem *rightButtonItem;
@property (nonatomic,assign) float adlResult,behResult,chcResult,currentRateResult;
@property (nonatomic,strong) DataStorageClass *myData;
@property (strong,nonatomic) ListOfRatesViewController *rateValuePicker;
@property (strong,nonatomic) UIPopoverController *rateValuePopover;
//@property (weak,nonatomic) id <CurrentCalcVCDelegate> delegate;
@property (strong,nonatomic) IBOutlet UILabel *currentRate;


-(IBAction)chooseDropDownRates:(UIButton*)sender;
-(IBAction)tapRatesButton:(UIButton *)sender;
-(void)computeAdlFunds:(int)val;
-(void)computeBehFunds:(int)val;
-(void)computeChcFunds:(int)val;

@end
//this protocol implementation pass the amount funding to New Rate View Controller
//@protocol CurrentCalcVCDelegate <NSObject>
//
//@required
//-(void)CurrentCaclViewController:(CurrentCalcVC*)currentVC didFinishChoosingRate:(float)myAmountFunding;
//
//@end
