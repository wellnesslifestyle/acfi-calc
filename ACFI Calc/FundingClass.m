//
//  FundingClass.m
//  fgawesgews
//
//  Created by V.NET on 9/24/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "FundingClass.h"

@implementation FundingClass


-(id)initWithRange:(int)range andCategory:(NSString *)category andFunding:(float)amount
{
    if(self=[super init]){
        _rangeValue = range;
        _myCategory = category;
        _myFunding = amount;
    }
    return self;
}

@end
