//
//  NSArray+PerformSelector.m
//  ACFI Calc
//
//  Created by V.NET on 12/1/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "NSArray+PerformSelector.h"
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

@implementation NSArray (PerformSelector)

- (NSArray *)arrayByPerformingSelector:(SEL)selector {
    NSMutableArray * results = [NSMutableArray array];
    
    for (id object in self) {
        id result = [object performSelector:selector];
        [results addObject:result];
    }
    
    return results;
}

@end
