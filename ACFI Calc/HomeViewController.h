//
//  HomeViewController2.h
//  ACFI Calc
//
//  Created by V.NET on 10/22/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HomeViewController : UIViewController


-(IBAction) clickOnLinkButton:(id)Sender;
-(IBAction)tapOnContact:(UIButton*)sender;
-(IBAction)tapOnCalculator:(UIButton*)sender;
@end
