//
//  SegmentsController.h
//  ACFI Calc
//
//  Created by V.NET on 12/1/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SegmentsController : NSObject

@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic, strong) UINavigationController *navigationController;
//@property (nonatomic, strong) UIBarButtonItem *navBackButton;

- (id)initWithNavigationController:(UINavigationController *)aNavigationController
                   viewControllers:(NSArray *)viewControllers;
//                      navBarButton:(UIBarButtonItem*)backButton;

- (void)indexDidChangeForSegmentedControl:(UISegmentedControl *)aSegmentedControl;

@end
