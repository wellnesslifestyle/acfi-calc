//
//  CellBackgroundView.h
//  fgawesgews
//
//  Created by V.NET on 10/4/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    CellPositionTop,
    CellPositionMiddle,
    CellPositionBottom,
    CellPositionSingle
} CellPosition;

@interface CellBackgroundView : UIView

@property(assign) CellPosition position;
@property(strong) UIColor *fillColor;
@property(strong) UIColor *borderColor;

@end
