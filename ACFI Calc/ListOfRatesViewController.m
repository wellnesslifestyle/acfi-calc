//
//  ListOfRatesViewController.m
//  fgawesgews
//
//  Created by V.NET on 9/23/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "ListOfRatesViewController.h"
#import "CurrentRates.h"
#import "CellBackgroundView.h"

@interface ListOfRatesViewController ()

@end

@implementation ListOfRatesViewController


#pragma mark - initialization method
- (id)init
{
    if(self = [super init]){
        _rateData = [[NSMutableArray alloc] init];
//        _myStorageData = [[DataStorageClass alloc] init];
    }
    return self;
}

//this will populate the array with RCS from RateTable Property List
-(void)setRateTable{
    NSDictionary *temp = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RateTable" ofType:@"plist"]];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for(int i=0;i < [temp count]; i++){
        NSString *rateKey = [[temp objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"0"];
        NSNumber *myFund = [[temp objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"1"];
        float amount = [myFund floatValue];
        
        CurrentRates *myRates = [[CurrentRates alloc] initWithRate:rateKey andAmount:amount];
        [tempArray addObject:myRates];
    }
    self.rateData = [NSMutableArray arrayWithArray:tempArray];
}


//Initializes a table-view controller to manage a table view of a given style.
- (id)initWithStyle:(UITableViewStyle)style
{
    
    if(self = [super initWithStyle:style]){
        self.clearsSelectionOnViewWillAppear = NO;
        
        self.contentSizeForViewInPopover = CGSizeMake(110, 190);
        
        //set my tableview background
        UIImageView *tableBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgColor"]];
        [tableBg setFrame:self.tableView.frame];
        self.tableView.backgroundView = tableBg;
        
        
    }
    return self;
}


#pragma mark - view life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    [self setRateTable];
    return [self.rateData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"table_cell_unselected_color"]];
      }
    cell.textLabel.text = [(CurrentRates*)[_rateData objectAtIndex:indexPath.row] myRate];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2f",((CurrentRates*)[_rateData objectAtIndex:indexPath.row]).amount];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    float fundingAmount;
    NSString *buttonLabel;
    
    [self setRateTable];
    fundingAmount = [(CurrentRates*)[_rateData objectAtIndex:indexPath.row] amount];
    buttonLabel = ((CurrentRates*)[_rateData objectAtIndex:indexPath.row]).myRate;
  
    //Notify the delegate if it exists.
    if (_delegate != nil) {
        [self.delegate ListOfRatesViewController:self updateRateFunding:fundingAmount andButtonLabel:buttonLabel];
    }
    
}

@end
