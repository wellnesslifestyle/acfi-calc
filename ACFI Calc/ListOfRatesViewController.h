//
//  ListOfRatesViewController.h
//  fgawesgews
//
//  Created by V.NET on 9/23/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataStorageClass.h"


@protocol ListOfRatesDelegate;


@interface ListOfRatesViewController : UITableViewController

@property (nonatomic,strong) NSMutableArray *rateData;

//@property (nonatomic,strong) DataStorageClass *myStorageData;
@property (nonatomic,weak) id<ListOfRatesDelegate> delegate;

-(void)setRateTable;
@end

@protocol ListOfRatesDelegate <NSObject>

@required
-(void) ListOfRatesViewController:(ListOfRatesViewController*)myListVC updateRateFunding:(float)myAmount andButtonLabel:(NSString*)myLabel;

@end