//
//  ContactViewController.m
//  fgawesgews
//
//  Created by V.NET on 10/8/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "ContactViewController.h"

@interface ContactViewController ()

@property (strong) IBOutlet UITextView *companyInfo;
@property (strong) IBOutlet UIImageView *contactImage;
@property (strong) IBOutlet UILabel *callUsLabel;
@property (strong) IBOutlet UILabel *mailLabel;
@property (strong) IBOutlet UITextView *contactNum;
@end


@implementation ContactViewController

-(void)customizeInterface{
//    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationItem setTitle:@"Contact Us"];
    self.navigationItem.hidesBackButton = YES;
    
    //customize left button 
    UIImage *leftButtonUnsel = [UIImage imageNamed:@"bar_button_exitUnselected"];
    UIImage *leftButtonSel = [UIImage imageNamed:@"bar_button_exitSelected"];
    UIButton *exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [exitButton setBackgroundImage:leftButtonSel forState:UIControlStateHighlighted];
    exitButton.frame = CGRectMake(0.0f, 0.0f,leftButtonUnsel.size.width,leftButtonUnsel.size.height);
    [exitButton setBackgroundImage:leftButtonUnsel forState:UIControlStateNormal];
    [exitButton addTarget:self
                        action:@selector(done:)
              forControlEvents:UIControlEventTouchUpInside];
    
	UIBarButtonItem *myModalBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:exitButton];
	self.navigationItem.rightBarButtonItem = myModalBarButtonItem;
    
    //set email link
    UITapGestureRecognizer *myMailGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMailViewController)];
    [self.myMail setEditable:NO];
    self.myMail.contentSize = CGSizeMake(self.myMail.bounds.size.width, self.myMail.bounds.size.height);
    [self.myMail setBackgroundColor:[UIColor clearColor]];
//    [self.myMail setDataDetectorTypes:UIDataDetectorTypeAll];
    [self.myMail setUserInteractionEnabled:YES];
    [self.myMail addGestureRecognizer:myMailGesture];
    
    //set contact number
    [self.contactNum setBackgroundColor:[UIColor clearColor]];
   
    
    if (IS_IPHONE5)
    {
        NSLog(@"4 inches iphone");
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor-568h"]];
//        [_companyInfo setBackgroundColor:[UIColor clearColor]];
        _companyInfo.translatesAutoresizingMaskIntoConstraints = NO;
        _contactImage.translatesAutoresizingMaskIntoConstraints = NO;
        _callUsLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _contactImage.translatesAutoresizingMaskIntoConstraints = NO;
        _myMail.translatesAutoresizingMaskIntoConstraints = NO;
        _mailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _contactNum.translatesAutoresizingMaskIntoConstraints = NO;
        
        //set contact number
        [self.contactNum setBackgroundColor:[UIColor clearColor]];

    
        // Add Status Bar and Navigation Bar heights together
        CGFloat navBarVerSpace = self.navigationController.navigationBar.frame.size.height;
        NSLog(@"Navigation Bar:%.2f",navBarVerSpace);
        
        //companyinfo pin to bottom navigaiton bar
        [self.view  addConstraint:[NSLayoutConstraint constraintWithItem:_companyInfo
                                                                          attribute:NSLayoutAttributeTop
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.view
                                                                          attribute:NSLayoutAttributeTop
                                                                         multiplier:1.0f
                                                                           constant:15.0f]];
        
        //companyInfo width
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companyInfo
                                                      attribute:NSLayoutAttributeWidth
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil
                                                      attribute:NSLayoutAttributeWidth
                                                     multiplier:1.0f
                                                       constant:298.0f]];

        //companyInfo height
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companyInfo
                                                      attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil
                                                      attribute:NSLayoutAttributeHeight
                                                     multiplier:1.0f
                                                       constant:119.0f]];

        //companyinfo align to x-axix
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companyInfo
                                                      attribute:NSLayoutAttributeCenterX
                                                      relatedBy:0
                                                         toItem:self.view
                                                      attribute:NSLayoutAttributeCenterX
                                                     multiplier:1.0f
                                                       constant:0.0f]];

        
        
        //callUsLabel pin vertical space to navBar
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_callUsLabel
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:_companyInfo
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0f
                                                               constant:15.0f]];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contactImage
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0f
                                                               constant:265.0f]];
        
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_myMail
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeWidth
                                                             multiplier:1.0f
                                                               constant:273.0f]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_myMail
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1.0f
                                                               constant:33.0f]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_myMail
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:_contactImage
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0f
                                                               constant:-10.0f]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_mailLabel
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:_myMail
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0f
                                                               constant:10.0f]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contactNum
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeWidth
                                                             multiplier:1.0f
                                                               constant:107.0f]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contactNum
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1.0f
                                                               constant:31.0f]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_callUsLabel
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:_contactNum
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0f
                                                               constant:10.0f]];
        
        
    }
    else{
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor.png"]];
        NSLog(@"3.5 inches iphone");
    }

    
}

- (id)init
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")){
        self=[super initWithNibName:@"ContactViewController" bundle:nil];
    }
    else
    {
        self=[super initWithNibName:@"ContactViewController-legacy" bundle:nil];
    }
    
   return self;
}


//this function let you send an email
-(void)tapMailViewController{
    //always check whether the current device is configured for sending emails
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
        
        mailVC.mailComposeDelegate = self;
        [mailVC setSubject:@"Hello!"];
        NSArray *mailRecipients = [NSArray arrayWithObjects:@"consultancy@wellnesslifestyles.com.au", nil];
        [mailVC setToRecipients:mailRecipients];
//         NSString *emailBody = @"";
//        [mailVC setMessageBody:emailBody isHTML:NO];
        [self presentModalViewController:mailVC animated:YES];
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Mail App needs to be installed on the device"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - this will dismiss Mail view controller
// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the
// message shown with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {

    
    UIAlertView *alertMsg;
    //Notifies users about errors associated with the interface
    switch (result)
	{
		case MFMailComposeResultCancelled:
            alertMsg = [[UIAlertView alloc] initWithTitle:nil message:@"Result: Mail sending canceled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertMsg show];
			break;
		case MFMailComposeResultSaved:
            alertMsg = [[UIAlertView alloc] initWithTitle:nil message:@"Result: Mail saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertMsg show];
			break;
		case MFMailComposeResultSent:
            alertMsg = [[UIAlertView alloc] initWithTitle:nil message:@"Result: Mail sent" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertMsg show];
			break;
		case MFMailComposeResultFailed:
            alertMsg = [[UIAlertView alloc] initWithTitle:nil message:@"Result: Mail sending failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertMsg show];
			break;
		default:
            alertMsg = [[UIAlertView alloc] initWithTitle:nil message:@"Result: Mail not sent" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertMsg show];
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}


-(void)done:(UIButton*)sender{
    [UIView animateWithDuration:0.80f
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.navigationController.view cache:NO];
                         }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self customizeInterface];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
