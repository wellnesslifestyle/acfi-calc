//
//  SummaryViewController.h
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalcViewController.h"

@interface SummaryViewController : UIViewController <CalcViewControllerDelegate>
{
    float currentRate,newRate,dailyRate,annualChange;
   
}


-(IBAction)goToLink:(id)sender;
-(IBAction)tapBack:(UIButton*)sender;
-(id)initWithCurrent:(float)myCurrent andNewRate:(float)myNew andRateChange:(float)myChange andAnnualChange:(float)myAnnual;

@end
