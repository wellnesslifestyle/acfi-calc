//
//  ChcViewController.h
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DataStorageClass;


//@protocol ChcViewDelegate;

@interface ChcViewController : UIViewController


@property (strong,nonatomic) DataStorageClass *myData;
//@property (weak,nonatomic) id<ChcViewDelegate> delegate;

//-(IBAction)goToCalcVC:(UIButton*)sender;
-(IBAction)tapButton:(UIButton*)sender;
-(void)determineCategoryAndAmount;
//-(IBAction)goToPrevVC:(UIButton*)sender;
@end

//@protocol ChcViewDelegate <NSObject>
//
//@required
//-(void)ChcViewController:(ChcViewController*)chcVH didSelectAdl:(float)myAdl didSelectBeh:(float)myBeh didSelectChc:(float)myChc;
