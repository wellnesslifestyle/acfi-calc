//
//  SummaryViewController.m
//  fgawesgews
//
//  Created by V.NET on 9/29/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "SummaryViewController.h"

@interface SummaryViewController ()
{
    IBOutlet UIImageView *currentHolder;
    IBOutlet UIImageView *newRateHolder;
    IBOutlet UIImageView *dailyHolder;
    IBOutlet UIImageView *annualHolder;


}
@property (strong,nonatomic) IBOutlet UILabel *currentLabel,*recentLabel,*dailyLabel,*annualLabel,*webLink;

@end

@implementation SummaryViewController


-(id)initWithCurrent:(float)myCurrent andNewRate:(float)myNew andRateChange:(float)myChange andAnnualChange:(float)myAnnual
{

    //this will check if the os version is 6 and about
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        //If it is at least 6.0, it loads the nib with Auto Layout enabled.
        self = [super initWithNibName:@"SummaryViewController" bundle:nil];
    }
    else
    {
        NSLog(@"version 5 to 5.9");
        self = [super initWithNibName:@"SummaryViewController-legacy" bundle:nil];
        
    }
    
    if(self){
        currentRate = myCurrent;
        newRate = myNew;
        dailyRate = myChange;
        annualChange = myAnnual;
    }
    return self;
}

-(void)CalcViewController:(CalcViewController *)calcVC updateColor:(UIColor *)myColor{

}


-(void)loadMyData{
    //assigning label with instance variable
   
    _currentLabel.text = [[NSString stringWithFormat:@"%c ",'$'] stringByAppendingFormat:@"%.2f",currentRate];
    _recentLabel.text = [[NSString stringWithFormat:@"%c ",'$'] stringByAppendingFormat:@"%.2f",newRate];
    _dailyLabel.text = [[NSString stringWithFormat:@"%c ",'$'] stringByAppendingFormat:@"%.2f",dailyRate];
    _annualLabel.text = [[NSString stringWithFormat:@"%c ",'$'] stringByAppendingFormat:@"%.2f",annualChange];
    
    if((newRate - currentRate) < 0){
        self.dailyLabel.textColor = self.annualLabel.textColor = [UIColor redColor];
    }
    
    
    
    
     //set the page background color
    if (IS_IPHONE5)
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor-568h"]];
    else
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor"]]];
    //    self.parentViewController.view.backgroundColor = [UIColor lightGrayColor];
    
    [currentHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"summaryRateHolder"]]];
     [newRateHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"summaryRateHolder"]]];
     [dailyHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"summaryRateHolder"]]];
     [annualHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"summaryRateHolder"]]];
    
    self.navigationItem.title = @"ACFI Rate Summary";
    
    //customized my left bar button
    UIImage *buttonSelected = [UIImage imageNamed:@"bar_left_buttonSelected"];
    UIImage *buttonUnselected = [UIImage imageNamed:@"bar_left_buttonUnselected"];
    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [myCustomButton setBackgroundImage:buttonSelected forState:UIControlStateHighlighted];
    [myCustomButton setBackgroundImage:buttonUnselected forState:UIControlStateNormal];
    myCustomButton.frame = CGRectMake(0.0f, 0.0f,buttonUnselected.size.width,buttonUnselected.size.height);
    [myCustomButton addTarget:self action:@selector(tapBack:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *myDoneButton = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
    self.navigationItem.leftBarButtonItem = myDoneButton;
    
    
    UITapGestureRecognizer *myLink = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToLink:)];
    [self.webLink setUserInteractionEnabled:YES];
    [self.webLink addGestureRecognizer:myLink];
    
}

-(void)goToLink:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.wlconsultancy.com.au"]];
    
}

-(IBAction)tapBack:(UIButton*)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - view controller life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadMyData];
}

#pragma mark - memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
