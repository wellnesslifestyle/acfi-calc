//
//  ViewController.m
//  fgawesgews
//
//  Created by V.NET on 9/16/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import "HomeViewController.h"
#import "ContactViewController.h"
#import "CalcViewController.h"



@interface HomeViewController ()

@property (strong) IBOutlet UIImageView *companyLogo;
@property (strong) IBOutlet UIImageView *companySlogan;
@property (strong) IBOutlet UIButton *websiteButton;
@property (strong) IBOutlet UIButton *calcButton;
@property (strong) IBOutlet UIButton *contactButton;

@end


@implementation HomeViewController

#pragma mark - link button
//this function will open a default browser to view the link
-(void) clickOnLinkButton:(id)Sender{
    
    UIButton * myButton = (UIButton *)Sender;
    if(myButton.tag == 0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.wlconsultancy.com.au"]];
    }
}


#pragma mark - Customized UI - private method
//private function that customize UI this will be called in viewDidload()
-(void)setInterfaceComponents{
    
    //set home button menu
    [self.calcButton setBackgroundImage:[UIImage imageNamed:@"calc_button.png"] forState:UIControlStateNormal];
    [self.websiteButton setBackgroundImage:[UIImage imageNamed:@"website_button"] forState:UIControlStateNormal];
    [self.contactButton setBackgroundImage:[UIImage imageNamed:@"contact_button"] forState:UIControlStateNormal];
    
    
    if (IS_IPHONE5){
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor-568h"]];
        
        _companyLogo.translatesAutoresizingMaskIntoConstraints = NO;
        _companySlogan.translatesAutoresizingMaskIntoConstraints = NO;
        _calcButton.translatesAutoresizingMaskIntoConstraints = NO;
        _websiteButton.translatesAutoresizingMaskIntoConstraints = NO;
        _contactButton.translatesAutoresizingMaskIntoConstraints = NO;
        //constrains for company logo in relation for view
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companyLogo
                                                                          attribute:NSLayoutAttributeTop
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.view
                                                                          attribute:NSLayoutAttributeTop
                                                                         multiplier:1.0f
                                                                           constant:28.0f]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companyLogo
                                                      attribute:NSLayoutAttributeCenterX
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.view
                                                      attribute:NSLayoutAttributeCenterX
                                                     multiplier:1.0f
                                                       constant:0.0f]];

        //company slogan constrain in relation for company logo
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companyLogo
                                                      attribute:NSLayoutAttributeBottom
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:_companySlogan
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1.0f
                                                       constant:-19.0f]];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companySlogan
                                                      attribute:NSLayoutAttributeCenterX
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:_companyLogo
                                                      attribute:NSLayoutAttributeCenterX
                                                     multiplier:1.0f
                                                       constant:0.0f]];

        
        //calculator button constrain
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companySlogan
                                                      attribute:NSLayoutAttributeBottom
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:_calcButton
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1.0f
                                                       constant:-33.0f]];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_companySlogan
                                                      attribute:NSLayoutAttributeCenterX
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:_calcButton
                                                      attribute:NSLayoutAttributeCenterX
                                                     multiplier:1.0f
                                                       constant:0.0f]];

        //website button constrain
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_calcButton
                                                      attribute:NSLayoutAttributeBottom
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:_websiteButton
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1.0
                                                       constant:-24.0f]];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_calcButton
                                                      attribute:NSLayoutAttributeCenterX
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:_websiteButton
                                                      attribute:NSLayoutAttributeCenterX
                                                     multiplier:1.0
                                                       constant:0.0f]];
        //contact button constrain
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_websiteButton
                                                      attribute:NSLayoutAttributeBottom
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:_contactButton
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1.0
                                                       constant:-24.0f]];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_websiteButton
                                                      attribute:NSLayoutAttributeCenterX
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:_contactButton
                                                      attribute:NSLayoutAttributeCenterX
                                                     multiplier:1.0
                                                       constant:0.0f]];

    }
    else{ //for 3.5 inches devices or below
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgColor"]];
    }
}


-(id)init{
    
    //this will check if the os version is 6 and about
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        //If it is at least 6.0, it loads the nib with Auto Layout enabled.
        self = [super initWithNibName:@"HomeViewController" bundle:nil];
    }
    else
    {
        NSLog(@"version 5 to 5.9");
        self = [super initWithNibName:@"HomeViewController-legacy" bundle:nil];
    }
    return self;
}


#pragma mark - View Controller LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setInterfaceComponents];
    
}

#pragma mark - view controller presentation
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    /*colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]*/
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar_bg"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar  setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor darkTextColor] ,UITextAttributeTextColor,
      [UIColor lightGrayColor],UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(1,2)],UITextAttributeTextShadowOffset,
      [UIFont fontWithName:@"Arial-Bold" size:0.0],UITextAttributeFont,
      nil]];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
        [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"bar_shadow"]];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


#pragma mark - Memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)tapOnCalculator:(UIButton*)sender{
    CalcViewController *calcVC = [[CalcViewController alloc] init];
    
    //checks if the navigation controller exists in view controller
    if (self.navigationController != nil){
        //animating view using blocks
        [UIView animateWithDuration:0.80f
                         animations:^{
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                             [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
//                             self.hidesBottomBarWhenPushed = YES;
                             [self.navigationController pushViewController:calcVC animated:YES];
//                                                      self.hidesBottomBarWhenPushed = NO;
                         }];
    }
}

-(IBAction)tapOnContact:(UIButton *)sender{
    
    ContactViewController *contactVC = [[ContactViewController alloc] init];
    
    
    //checks if the navigation controller exists in view controller
    if (self.navigationController != nil){
        //animating view using blocks
        [UIView animateWithDuration:0.80f
                         animations:^{
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                             [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
//                             self.hidesBottomBarWhenPushed = YES;
                             [self.navigationController pushViewController:contactVC animated:YES];
//                             self.hidesBottomBarWhenPushed = NO;
                         }];
    }
    
}

@end
