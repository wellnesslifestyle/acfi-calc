//
//  DataAPI.h
//  ACFI Calc
//
//  Created by V.NET on 10/14/13.
//  Copyright (c) 2013 USC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataAPI : NSObject
{
    float _currentAmount,_adlAmount,_behAmount,_chcAmount;
}

+(DataAPI*)sharedData;

-(float)getMyCurrentRate;
-(void)setMyCurrentRate:(float)myRate;

-(void)setMyAdlRate:(float)myAdl;
-(float)getMyAdlRate;
-(void)setMyBehRate:(float)myBeh;
-(float)getMyBehRate;
-(void)setMyChcRate:(float)myChc;
-(float)getMyChcRate;

-(void)resetCurrent:(float)cur andADL:(float)adl andBeh:(float)beh andChc:(float)chc;
@end
