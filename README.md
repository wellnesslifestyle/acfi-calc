ACFI-Calc
=========

Allow you to quickly and easily calculate a resident’s ACFI rate based on their current care needs.
